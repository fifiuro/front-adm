import { Component } from '@angular/core';
import { ReportesService } from 'src/app/services/reportes/reportes.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent {
  buscar = { fInicio: '2023-01-01', fFin: '2023-12-31' };
  cargando: boolean = true;
  reporte: any = [];

  constructor(private readonly reporteService: ReportesService) {}

  ngOnInit() {
    this.findReporteLoad();
  }

  findReporteLoad() {
    this.cargando = false;
    if (this.buscar.fInicio && this.buscar.fFin) {
      this.reporteService
        .getReporteLoadData(this.buscar.fInicio, this.buscar.fFin)
        .subscribe({
          next: (res: any) => {
            if (res) {
              this.reporte = res;
            }
          },
        });
    }
  }
}
