import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-afiliado',
  templateUrl: './afiliado.component.html',
  styleUrls: ['./afiliado.component.css'],
})
export class AfiliadoComponent {
  @Input() header: any;
  @Input() show: boolean = false;
}
