import { Component, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { RolesService } from 'src/app/services/roles/roles.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent {
  @Output() menu = new EventEmitter();
  nomUsuario: string = '';
  nomCompleto: string = '';
  selectedRol!: number;
  roles = [];

  constructor(
    private router: Router,
    private readonly rolesService: RolesService,
    private readonly authService: AuthService
  ) {}

  ngOnInit() {
    this.nomUsuario = this.authService.getData('nom_usuario');
    this.nomCompleto = this.authService.getData('nom_completo');
    this.rolesService.getRoles(this.authService.getData('rol')).subscribe({
      next: (val: any) => {
        if (val) {
          this.roles = val;
        }
      },
    });
  }
  logout() {
    this.authService.clearAllData();
    this.router.navigate(['public/']);
  }

  changeRol() {
    if (this.selectedRol != null) {
      this.menu.emit(this.selectedRol);
    } else {
      this.router.navigate(['/pages/']);
      this.menu.emit('');
    }
  }
}
