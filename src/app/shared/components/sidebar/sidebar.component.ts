import { Component, Input } from '@angular/core';
import { PermisosService } from 'src/app/services/permisos/permisos.service';
declare var $: any;

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css'],
})
export class SidebarComponent {
  @Input() menu: any;
  @Input() refresh: any = false;
  showMenu: boolean = true;

  constructor(private readonly permisosService: PermisosService) {}

  ngOnInit() {}

  ngOnChanges() {
    if (this.refresh) {
      if (this.menu.length > 0) {
        this.showMenu = false;
      } else {
        this.showMenu = true;
      }
      $('[data-widget="treeview"]').Treeview('init');
    }
  }

  selecionarPermisos(permisos: string) {
    this.permisosService.asignarPermisos(permisos);
  }
}
