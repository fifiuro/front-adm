import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { LoginBoxComponent } from './components/login-box/login-box.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { NotFoundComponent } from './components/pagina-error/not-found/not-found.component';
import { ErrorPageComponent } from './components/pagina-error/error-page/error-page.component';
import { HomeComponent } from './components/home/home.component';
import { CargandoComponent } from './components/cargando/cargando.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModule } from '@angular/common';
import { LoadTableComponent } from './components/load-table/load-table.component';
import { AfiliadoComponent } from './components/afiliado/afiliado.component';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    NgSelectModule,
    CommonModule,
  ],
  declarations: [
    LoginBoxComponent,
    NavbarComponent,
    SidebarComponent,
    NotFoundComponent,
    ErrorPageComponent,
    HomeComponent,
    CargandoComponent,
    LoadTableComponent,
    AfiliadoComponent,
  ],
  exports: [
    LoginBoxComponent,
    NavbarComponent,
    SidebarComponent,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    HomeComponent,
    CargandoComponent,
    LoadTableComponent,
    AfiliadoComponent,
  ],
  providers: [],
})
export class SharedModule {
  constructor() {}
}
