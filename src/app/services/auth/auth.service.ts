import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Env } from 'src/environments/env';
import * as CryptoJS from 'crypto-js';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private readonly baseUrlSistema: string = Env.urlSistema;
  private readonly urlLogin: string = Env.urlLogin;
  private readonly key = Env.key;

  constructor(private http: HttpClient) {}

  login(user: string, pass: string): Observable<any> {
    return this.http.post<any>(this.urlLogin + 'auth/loginSistemas', {
      username: user,
      password: pass,
    });
  }

  loginSistema(user: string, pass: string): Observable<any> {
    return this.http.post<any>(this.baseUrlSistema + 'security/login', {
      user: Env.userSistema,
      password: Env.passSistema,
    });
  }

  public saveData(key: string, value: string) {
    localStorage.setItem(key, this.encrypt(value));
  }

  private encrypt(txt: string): string {
    return CryptoJS.AES.encrypt(txt, this.key).toString();
  }

  private decrypt(txtToDecrypt: string) {
    return CryptoJS.AES.decrypt(txtToDecrypt, this.key).toString(
      CryptoJS.enc.Utf8
    );
  }

  public getData(key: string) {
    let data = localStorage.getItem(key) || '';
    return this.decrypt(data);
  }

  public clearAllData() {
    localStorage.clear();
  }

  VerificarJWT(): boolean {
    if (this.getData('jwt') && this.getData('jwtSis')) {
      return true;
    }
    return false;
  }
}
