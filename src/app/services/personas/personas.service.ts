import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PersonasInterface } from 'src/app/interfaces';
import { Env } from 'src/environments/env';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root',
})
export class PersonasService {
  private readonly baseUrl: string = Env.baseUrl;
  private readonly api: string = '/persona/';
  public refresh!: boolean;

  constructor(
    private http: HttpClient,
    private readonly authService: AuthService
  ) {}

  postAddPersona(data: any): Observable<PersonasInterface> {
    this.authService.saveData('change', '1');
    return this.http.post(this.baseUrl + this.api + 'add', data);
  }

  getByIdPersona(id: number): Observable<PersonasInterface> {
    this.authService.saveData('change', '1');
    return this.http.get(this.baseUrl + this.api + 'id/' + id);
  }
}
