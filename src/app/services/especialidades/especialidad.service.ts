import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, delay } from 'rxjs';
import { EspecialidadesInterface } from 'src/app/interfaces/especialidades/especialidades.interface';
import { Env } from 'src/environments/env';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root',
})
export class EspecialidadService {
  private readonly baseUrl: string = Env.baseUrl;
  private readonly api: string = '/especialidad/';
  public refresh!: boolean;
  public data!: EspecialidadesInterface;

  constructor(
    private http: HttpClient,
    private readonly authService: AuthService
  ) {}

  getEspecialidades(
    page: number,
    cantidad: number
  ): Observable<EspecialidadesInterface[]> {
    this.authService.saveData('change', '1');
    return this.http.get<EspecialidadesInterface[]>(
      this.baseUrl + this.api + 'all?limit=' + cantidad + '&offset=' + page
    );
  }

  getLikeExpecialidad(
    page: number,
    cantidad: number,
    texto: string
  ): Observable<EspecialidadesInterface[]> {
    this.authService.saveData('change', '1');
    return this.http.get<EspecialidadesInterface[]>(
      this.baseUrl +
        this.api +
        'like/' +
        texto +
        '?limit=' +
        cantidad +
        '&offset=' +
        page
    );
  }

  getComboEsepecialidad(): Observable<EspecialidadesInterface[]> {
    this.authService.saveData('change', '1');
    return this.http.get<EspecialidadesInterface[]>(
      this.baseUrl + this.api + 'combo'
    );
  }

  getByIDEspecialidad(id: number): Observable<EspecialidadesInterface> {
    this.authService.saveData('change', '1');
    return this.http.get<EspecialidadesInterface>(
      this.baseUrl + this.api + 'id/' + id
    );
  }

  postAddEspecialidad(data: any): Observable<EspecialidadesInterface> {
    this.authService.saveData('change', '1');
    return this.http.post(this.baseUrl + this.api + 'add', data);
  }

  putUpdateEspecialidad(id: number, data: any): Observable<any> {
    this.authService.saveData('change', '1');
    return this.http.put(this.baseUrl + this.api + 'update/' + id, data);
  }
}
