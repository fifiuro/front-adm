import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CaratulasInterface } from 'src/app/interfaces/index';
import { Env } from 'src/environments/env';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root',
})
export class CaratulasService {
  private readonly baseUrlSistema: string = Env.urlSistema;
  private readonly baseUrl: string = Env.baseUrl;
  private readonly api: string = '/caratula/';
  public refresh!: boolean;
  public data!: CaratulasInterface;

  constructor(
    private http: HttpClient,
    private readonly authService: AuthService
  ) {}

  getCaratulas(
    page: number,
    cantidad: number
  ): Observable<CaratulasInterface[]> {
    this.authService.saveData('change', '1');
    return this.http.get<CaratulasInterface[]>(
      this.baseUrl + this.api + 'all?limit=' + cantidad + '&offset=' + page
    );
  }

  getByIdCaratula(id: number): Observable<CaratulasInterface> {
    this.authService.saveData('change', '1');
    return this.http.get<CaratulasInterface>(
      this.baseUrl + this.api + 'id/' + id
    );
  }

  getByRutaCaratula(ruta: string): Observable<CaratulasInterface> {
    this.authService.saveData('change', '1');
    return this.http.get<CaratulasInterface>(
      this.baseUrl + this.api + 'ruta/' + ruta
    );
  }

  getByRutaLike(ruta: string): Observable<number> {
    this.authService.saveData('change', '1');
    return this.http.get<number>(this.baseUrl + this.api + 'rutaLike/' + ruta);
  }

  getBuscar(
    id: number,
    tipoAtencion: number,
    especialidad: number,
    fInicio: string,
    fFin: string
  ): Observable<CaratulasInterface[]> {
    this.authService.saveData('change', '1');
    return this.http.get<CaratulasInterface[]>(
      this.baseUrl +
        this.api +
        'buscar/' +
        id +
        '/' +
        tipoAtencion +
        '/' +
        especialidad +
        '/' +
        fInicio +
        '/' +
        fFin
    );
  }

  postAddCaratula(data: any): Observable<any> {
    this.authService.saveData('change', '1');
    return this.http.post(this.baseUrl + this.api + 'add', data);
  }

  putUpdateCaratula(id: number, data: any): Observable<any> {
    this.authService.saveData('change', '1');
    return this.http.put(this.baseUrl + this.api + 'update/' + id, data);
  }

  getFindCie(texto: string) {
    this.authService.saveData('change', '1');
    return this.http.get<any[]>(
      this.baseUrlSistema + 'gestion/getDiagnosticoCie10ByParam/' + texto
    );
  }

  putEliminarCaratula(ruta: string, data: any): Observable<any> {
    this.authService.saveData('change', '1');
    return this.http.put(this.baseUrl + this.api + 'eliminar/' + ruta, data);
  }
}
