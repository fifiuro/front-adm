import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { MedicosInterface } from 'src/app/interfaces';
import { Env } from 'src/environments/env';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root',
})
export class MedicosService {
  private readonly baseUrl: string = Env.baseUrl;
  private readonly api: string = '/medico/';
  public refresh!: boolean;

  constructor(
    private http: HttpClient,
    private readonly authService: AuthService
  ) {}

  postAddMedico(data: any): Observable<MedicosInterface> {
    this.authService.saveData('change', '1');
    return this.http.post(this.baseUrl + this.api + 'add', data);
  }

  getByIdMedico(id: number): Observable<MedicosInterface> {
    this.authService.saveData('change', '1');
    return this.http.get(this.baseUrl + this.api + 'id/' + id);
  }

  getVerificaMedico(
    nombre: string,
    paterno: string,
    materno: string,
    especialidad: string
  ) {
    this.authService.saveData('change', '1');
    return this.http.get(
      this.baseUrl +
        this.api +
        'verifica/' +
        nombre +
        '/' +
        paterno +
        '/' +
        materno +
        '/' +
        especialidad
    );
  }
}
