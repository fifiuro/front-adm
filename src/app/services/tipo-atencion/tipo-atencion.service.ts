import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, delay, throwError } from 'rxjs';
import { TipoAtencionInterface } from 'src/app/interfaces/tipos-atencion/tipo-atencion.interface';
import { Env } from '../../../environments/env';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root',
})
export class TipoAtencionService {
  private readonly baseUrl: string = Env.baseUrl;
  public refresh!: boolean;
  public data!: TipoAtencionInterface;

  constructor(
    private http: HttpClient,
    private readonly authService: AuthService
  ) {}

  getTipoAtencion(
    page: number,
    cantidad: number
  ): Observable<TipoAtencionInterface[]> {
    this.authService.saveData('change', '1');
    return this.http.get<TipoAtencionInterface[]>(
      this.baseUrl + '/tipo-atencion/all?limit=' + cantidad + '&offset=' + page
    );
    // .pipe(delay(2000));
  }

  getLikeTipoaAtencion(
    page: number,
    cantidad: number,
    texto: string
  ): Observable<TipoAtencionInterface[]> {
    this.authService.saveData('change', '1');
    return this.http.get<TipoAtencionInterface[]>(
      this.baseUrl +
        '/tipo-atencion/like/' +
        texto +
        '?limit=' +
        cantidad +
        '&offset=' +
        page
    );
  }

  getComboTipoAtencion(): Observable<TipoAtencionInterface[]> {
    this.authService.saveData('change', '1');
    return this.http.get<TipoAtencionInterface[]>(
      this.baseUrl + '/tipo-atencion/combo'
    );
  }

  getByIDTipoAtencion(id: number): Observable<TipoAtencionInterface> {
    this.authService.saveData('change', '1');
    return this.http.get<TipoAtencionInterface>(
      this.baseUrl + '/tipo-atencion/id/' + id
    );
  }

  postAddTipoatencion(data: any): Observable<TipoAtencionInterface> {
    this.authService.saveData('change', '1');
    return this.http.post(this.baseUrl + '/tipo-atencion/add', data);
  }

  putUpdateTipoAtencion(id: number, data: any): Observable<any> {
    this.authService.saveData('change', '1');
    return this.http.put(this.baseUrl + '/tipo-atencion/update/' + id, data);
  }
}
