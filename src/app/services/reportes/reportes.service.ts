import { HttpBackend, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map, pipe } from 'rxjs';
import { Env } from 'src/environments/env';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root',
})
export class ReportesService {
  private readonly baseUrl: string = Env.baseUrl;
  private readonly api: string = '/reportes/';
  public refresh!: boolean;
  public data!: any;

  constructor(
    private http: HttpClient,
    private readonly authService: AuthService
  ) {}

  getReporteLoadData(fInicio: string, fFin: string): Observable<any[]> {
    this.authService.saveData('change', '1');
    return this.http.get<any[]>(
      this.baseUrl + this.api + 'tipo/' + fInicio + '/' + fFin
    );
  }

  getDownloadReporteConteo(fInicio: string, fFin: string) {
    this.authService.saveData('change', '1');
    return this.http.get(
      this.baseUrl + this.api + 'download/' + fInicio + '/' + fFin,
      { responseType: 'blob' }
    );
  }
}
