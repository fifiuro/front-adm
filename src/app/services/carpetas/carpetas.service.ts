import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, delay } from 'rxjs';
import { CarpetasInterface } from 'src/app/interfaces/carpetas/carpetas.interfaces';
import { Env } from 'src/environments/env';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root',
})
export class CarpetasService {
  private readonly baseUrl: string = Env.baseUrl;
  private readonly api: string = '/carpeta/';
  public refresh!: boolean;
  public data!: CarpetasInterface;

  constructor(
    private http: HttpClient,
    private readonly authService: AuthService
  ) {}

  getCarpetas(page: number, cantidad: number): Observable<CarpetasInterface[]> {
    this.authService.saveData('change', '1');
    return this.http.get<any>(
      this.baseUrl + this.api + 'all?limit=' + cantidad + '&offset=' + page
    );
  }

  getLikeCarpeta(
    page: number,
    cantidad: number,
    texto: string
  ): Observable<CarpetasInterface[]> {
    this.authService.saveData('change', '1');
    return this.http.get<CarpetasInterface[]>(
      this.baseUrl +
        this.api +
        'like/' +
        texto +
        '?limit=' +
        cantidad +
        '&offset=' +
        page
    );
  }

  getComboCarpeta(): Observable<CarpetasInterface[]> {
    this.authService.saveData('change', '1');
    return this.http.get<CarpetasInterface[]>(
      this.baseUrl + this.api + 'combo'
    );
  }

  getByIDCarpeta(id: number): Observable<CarpetasInterface> {
    this.authService.saveData('change', '1');
    return this.http.get<CarpetasInterface>(
      this.baseUrl + this.api + 'id/' + id
    );
  }

  postAddCarpeta(data: any): Observable<CarpetasInterface> {
    this.authService.saveData('change', '1');
    return this.http.post(this.baseUrl + this.api + 'add', data);
  }

  putUpdateCarpeta(id: number, data: any): Observable<any> {
    this.authService.saveData('change', '1');
    return this.http.put(this.baseUrl + this.api + 'update/' + id, data);
  }
}
