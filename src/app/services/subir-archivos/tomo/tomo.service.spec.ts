import { TestBed } from '@angular/core/testing';

import { TomoService } from './tomo.service';

describe('TomoService', () => {
  let service: TomoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TomoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
