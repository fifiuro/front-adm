import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TomoInterface } from 'src/app/interfaces/tomo/tomo.interface';
import { Env } from 'src/environments/env';
import { AuthService } from '../../auth/auth.service';

@Injectable({
  providedIn: 'root',
})
export class TomoService {
  private readonly baseUrl: string = Env.baseUrl;
  private readonly api: String = '/tomo/';

  constructor(
    private http: HttpClient,
    private readonly authService: AuthService
  ) {}

  getTomo(page: number, cantidad: number): Observable<TomoInterface[]> {
    this.authService.saveData('change', '1');
    return this.http.get<TomoInterface[]>(
      this.baseUrl + this.api + 'all?limit=' + cantidad + '&offset=' + page
    );
  }

  getByIdTomo(id: number): Observable<TomoInterface> {
    this.authService.saveData('change', '1');
    return this.http.get<TomoInterface>(this.baseUrl + this.api + 'id/' + id);
  }

  getByIdPersona(id: number): Observable<TomoInterface> {
    this.authService.saveData('change', '1');
    return this.http.get<TomoInterface>(
      this.baseUrl + this.api + 'persona/' + id
    );
  }

  getComboTomo(id: number): Observable<TomoInterface> {
    this.authService.saveData('change', '1');
    return this.http.get<TomoInterface>(
      this.baseUrl + this.api + 'combo' + '/' + id
    );
  }

  postAddTomo(data: any): Observable<TomoInterface> {
    this.authService.saveData('change', '1');
    return this.http.post(this.baseUrl + this.api + 'add', data);
  }

  putupdateFojas(id: number, data: any): Observable<any> {
    this.authService.saveData('change', '1');
    return this.http.put(this.baseUrl + this.api + 'updateFojas/' + id, data);
  }
}
