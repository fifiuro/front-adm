import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Env } from 'src/environments/env';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root',
})
export class SubirArchivosService {
  private readonly baseUrlSistema: string = Env.urlSistema;

  constructor(
    private http: HttpClient,
    private readonly authService: AuthService
  ) {}

  getFindAfiliado(ci: string, matricula: string) {
    this.authService.saveData('change', '2');
    return this.http.get<any[]>(
      this.baseUrlSistema +
        'modelo/getDatosAseguradoByAseMatCi/' +
        matricula +
        '/' +
        ci
    );
  }
}
