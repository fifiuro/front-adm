import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Env } from 'src/environments/env';
import { AuthService } from '../../auth/auth.service';

@Injectable({
  providedIn: 'root',
})
export class FtpService {
  private readonly baseUrl: string = Env.baseUrl;
  private readonly api: string = '/ftp/';
  constructor(
    private http: HttpClient,
    private readonly authService: AuthService
  ) {}

  createStructure(ruta: string, carpeta: string): Observable<any> {
    this.authService.saveData('change', '1');
    return this.http.get<any>(
      this.baseUrl + this.api + 'createStruct/' + ruta + '/' + carpeta
    );
  }

  createFolders(
    ruta: string,
    matricula: string,
    tomo: string,
    carpetas: string
  ) {
    this.authService.saveData('change', '1');
    return this.http.get<any>(
      this.baseUrl +
        this.api +
        'createFolders/' +
        ruta +
        '/' +
        matricula +
        '/' +
        tomo +
        '/' +
        carpetas
    );
  }

  createTomo(ruta: string, matricula: string, tomo: string, carpetas: string) {
    this.authService.saveData('change', '1');
    return this.http.get<any>(
      this.baseUrl +
        this.api +
        'createTomo/' +
        ruta +
        '/' +
        matricula +
        '/' +
        tomo +
        '/' +
        carpetas
    );
  }

  getListFolder(ruta: string): Observable<any> {
    this.authService.saveData('change', '1');
    return this.http.get<any[]>(this.baseUrl + this.api + 'list/' + ruta);
  }

  getBackFolder(ruta: string): Observable<any> {
    this.authService.saveData('change', '1');
    return this.http.get<any[]>(this.baseUrl + this.api + 'back/' + ruta);
  }

  renemaFolder(nombreActual: string, nuevoNombre: string) {
    this.authService.saveData('change', '1');
    return this.http.get<any>(
      this.baseUrl +
        this.api +
        'renameFolder/' +
        nombreActual +
        '/' +
        nuevoNombre
    );
  }

  deleteFolder(ruta: string, nombre: string, tipo: string): Observable<any> {
    this.authService.saveData('change', '1');
    return this.http.get<any>(
      this.baseUrl + this.api + 'delete/' + ruta + '/' + nombre + '/' + tipo
    );
  }

  upFile(ruta: string, file: any) {
    this.authService.saveData('change', '1');
    const formularioDeDatos = new FormData();
    formularioDeDatos.append('file', file);

    return this.http.post<any>(
      this.baseUrl + this.api + 'upload/' + ruta,
      formularioDeDatos
    );
  }

  downloadFileFTP(rutaFtp: string) {
    this.authService.saveData('change', '1');
    return this.http.get<any>(this.baseUrl + this.api + 'download/' + rutaFtp);
  }

  downloadFile(fileName: string) {
    this.authService.saveData('change', '1');
    return this.http.get(this.baseUrl + this.api + 'archivo/' + fileName, {
      responseType: 'blob',
    });
  }
}
