import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Env } from 'src/environments/env';
import { AuthService } from '../auth/auth.service';
import { RoleInterface } from 'src/app/interfaces';

@Injectable({
  providedIn: 'root',
})
export class RolesService {
  private readonly baseUrlLogin: string = Env.urlLogin;

  constructor(
    private readonly http: HttpClient,
    private readonly authService: AuthService
  ) {}

  getRoles(roles: string) {
    this.authService.saveData('change', '2');
    return this.http.get<RoleInterface[]>(
      this.baseUrlLogin + 'roles/comboIn/' + roles
    );
  }

  getMenu(rol: string) {
    this.authService.saveData('change', '2');
    return this.http.get<any>(this.baseUrlLogin + 'roles/find-menu-rol/' + rol);
  }
}
