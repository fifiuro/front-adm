import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, delay } from 'rxjs';
import { PrestamosInterface } from 'src/app/interfaces/prestamos/prestamos.interface';
import { Env } from 'src/environments/env';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root',
})
export class PrestamosService {
  private readonly baseUrlSistema: string = Env.urlSistema;
  private readonly baseUrl: string = Env.baseUrl;
  private readonly api: string = '/prestamos/';
  public refresh!: boolean;

  constructor(
    private http: HttpClient,
    private readonly authService: AuthService
  ) {}

  getFindMedico(paterno: string, nombre: string) {
    this.authService.saveData('change', '2');
    return this.http.get<PrestamosInterface[]>(
      this.baseUrlSistema + 'cita/getMedicoByApatNom/' + paterno + '/' + nombre
    );
  }

  getPrestamos(
    page: number,
    cantidad: number
  ): Observable<PrestamosInterface[]> {
    this.authService.saveData('change', '1');
    return this.http
      .get<PrestamosInterface[]>(
        this.baseUrl + this.api + 'all?limit=' + cantidad + '&offset=' + page
      );
  }

  getFindPrestamosX3(
    page: number,
    cantidad: number,
    buscar: any
  ): Observable<PrestamosInterface[]> {
    this.authService.saveData('change', '1');
    return this.http.get<PrestamosInterface[]>(
      this.baseUrl +
        this.api +
        'prestamox3?afiliado=' +
        buscar.afialiado +
        '&medico=' +
        buscar.medico +
        '&estado=' +
        buscar.estado +
        '&limit=' +
        cantidad +
        '&offset=' +
        page
    );
  }

  getFindByIdPrestamo(id: number): Observable<PrestamosInterface> {
    this.authService.saveData('change', '1');
    return this.http.get<PrestamosInterface>(
      this.baseUrl + this.api + 'id/' + id
    );
  }

  postAddPrestamo(data: any): Observable<any> {
    this.authService.saveData('change', '1');
    return this.http.post(this.baseUrl + this.api + 'add', data);
  }

  putUpdatePrestamo(id: number, data: any): Observable<any> {
    this.authService.saveData('change', '1');
    return this.http.put(this.baseUrl + this.api + 'update/' + id, data);
  }

  fecha(): Observable<any> {
    this.authService.saveData('change', '1');
    return this.http.get(this.baseUrl + this.api + 'fecha');
  }
}
