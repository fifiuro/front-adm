import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { PersonaUnidadInterface } from 'src/app/interfaces/personaUnidad/personaUnidad.interface';
import { Env } from 'src/environments/env';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root',
})
export class PersonaUnidadService {
  private readonly baseUrl: string = Env.baseUrl;
  private readonly api: string = '/persona-unidad/';

  constructor(
    private http: HttpClient,
    private readonly authService: AuthService
  ) {}

  getPersonalUnidad(
    page: number,
    cantidad: number
  ): Observable<PersonaUnidadInterface[]> {
    this.authService.saveData('change', '1');
    return this.http.get<PersonaUnidadInterface[]>(
      this.baseUrl + this.api + 'all?limit=' + cantidad + '&offset=' + page
    );
  }

  postAddPersonaUnidad(data: any): Observable<PersonaUnidadInterface> {
    this.authService.saveData('change', '1');
    return this.http.post(this.baseUrl + this.api + 'add', data);
  }

  putUpdatePersonaUnidad(id: number, data: any): Observable<any> {
    this.authService.saveData('change', '1');
    return this.http.put(this.baseUrl + this.api + 'update/' + id, data);
  }

  getLikePersonaUnidad(
    page: number,
    cantidad: number,
    texto: any
  ): Observable<PersonaUnidadInterface[]> {
    this.authService.saveData('change', '1');
    return this.http.get<PersonaUnidadInterface[]>(
      this.baseUrl +
        this.api +
        'like?matricula=' +
        texto.matricula +
        '&ci=' +
        texto.ci +
        '&limit=' +
        cantidad +
        '&offset=' +
        page
    );
  }

  getByIdPersonaUnidad(id: number): Observable<PersonaUnidadInterface> {
    this.authService.saveData('change', '1');
    return this.http.get<PersonaUnidadInterface>(
      this.baseUrl + this.api + 'id/' + id
    );
  }
}
