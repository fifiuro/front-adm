import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class PermisosService {
  permisos: string = '';
  constructor() {}

  asignarPermisos(permisos: string) {
    this.permisos = permisos;
  }
  verificarPermisos(compara: string) {
    return this.permisos.includes(compara);
  }
}
