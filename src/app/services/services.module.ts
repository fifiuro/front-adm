import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { TipoAtencionService } from '.';
import { HttpErrorInterceptorInterceptor } from '../interceptors/http-error-interceptor.interceptor';
import { EspecialidadService } from './especialidades/especialidad.service';
import { UnidadesService } from './unidades/unidades.service';
import { CarpetasService } from './carpetas/carpetas.service';
import { SubirArchivosService } from './subir-archivos/subir-archivos.service';
import { JwtInterceptor } from '../interceptors/jwt/jwt.interceptor';

@NgModule({
  imports: [HttpClientModule],
  providers: [
    TipoAtencionService,
    EspecialidadService,
    UnidadesService,
    CarpetasService,
    SubirArchivosService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptorInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true,
    },
  ],
})
export class ServicesModule {}
