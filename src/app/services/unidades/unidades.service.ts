import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CarpetasInterface } from 'src/app/interfaces/carpetas/carpetas.interfaces';
import { EspecialidadesInterface } from 'src/app/interfaces/especialidades/especialidades.interface';
import { UnidadesInterface } from 'src/app/interfaces/unidades/especialidades.interface';
import { Env } from 'src/environments/env';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root',
})
export class UnidadesService {
  private readonly baseUrl: string = Env.baseUrl;
  private readonly api: string = '/unidad/';
  public refresh!: boolean;
  public data!: EspecialidadesInterface;

  constructor(
    private http: HttpClient,
    private readonly authService: AuthService
  ) {}

  getUnidad(page: number, cantidad: number): Observable<UnidadesInterface[]> {
    this.authService.saveData('change', '1');
    return this.http.get<UnidadesInterface[]>(
      this.baseUrl + this.api + 'all?limit=' + cantidad + '&offset=' + page
    );
  }

  getLikeUnidad(
    page: number,
    cantidad: number,
    texto: string
  ): Observable<UnidadesInterface[]> {
    this.authService.saveData('change', '1');
    return this.http.get<UnidadesInterface[]>(
      this.baseUrl +
        this.api +
        'like/' +
        texto +
        '?limit=' +
        cantidad +
        '&offset=' +
        page
    );
  }

  getByIDUnidad(id: number): Observable<UnidadesInterface> {
    this.authService.saveData('change', '1');
    return this.http.get<UnidadesInterface>(
      this.baseUrl + this.api + 'id/' + id
    );
  }

  getComboUnidad(): Observable<CarpetasInterface> {
    this.authService.saveData('change', '1');
    return this.http.get<CarpetasInterface>(this.baseUrl + this.api + 'combo');
  }

  postAddUnidad(data: any): Observable<UnidadesInterface> {
    this.authService.saveData('change', '1');
    return this.http.post(this.baseUrl + this.api + 'add', data);
  }

  putUpdateUnidad(id: number, data: any): Observable<any> {
    this.authService.saveData('change', '1');
    return this.http.put(this.baseUrl + this.api + 'update/' + id, data);
  }
}
