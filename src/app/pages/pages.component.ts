import { Component } from '@angular/core';
import { RolesService } from '../services/roles/roles.service';
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.css'],
})
export class PagesComponent {
  public cargando: boolean = true;
  menu: any;
  refresh = false;

  constructor(
    private readonly rolServicio: RolesService,
    private router: Router
  ) {}

  ngOnInit() {
    if (this.menu) {
    } else {
      this.router.navigate(['/pages/']);
    }
    // if(this.menu)
    $('[data-widget="treeview"]').Treeview('init');
    $('[data-widget="nav-treeview"]').Treeview('init');
  }

  receiveRol($event: any) {
    if ($event) {
      this.refresh = false;
      this.rolServicio.getMenu($event).subscribe({
        next: (val: any) => {
          if (val.status) {
            this.menu = val.data;
            this.refresh = true;
          }
        },
      });
    } else {
      this.menu = [];
      this.refresh = true;
    }
  }
}
