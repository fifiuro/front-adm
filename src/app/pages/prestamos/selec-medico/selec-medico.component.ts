import { Component, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AlertsService } from 'src/app/helpers/alerts/alerts.service';
import { PrestamosService } from 'src/app/services/prestamos/prestamos.service';
declare var $: any;

@Component({
  selector: 'app-selec-medico',
  templateUrl: './selec-medico.component.html',
  styleUrls: ['./selec-medico.component.css'],
})
export class SelecMedicoComponent {
  @Output() medico = new EventEmitter();
  cargando: boolean = false;
  isClicked: boolean = false;
  medicos: any = [];
  pagination: number = 1;
  cantReg: number = 10;
  allMedicos: number = 0;
  public hasError: boolean = false;
  
  constructor(
    private readonly prestamosService: PrestamosService,
    private alert: AlertsService
  ) {}

  get paterno() {
    return this.formMedicos.get('paterno') as FormControl;
  }

  get nombre() {
    return this.formMedicos.get('nombre') as FormControl;
  }

  formMedicos = new FormGroup({
    paterno: new FormControl('', Validators.required),
    nombre: new FormControl('', Validators.required),
  });

  findMedicos() {
    this.medicos.splice(0, this.medicos.length);
    this.isClicked = true;
    this.cargando = false;
    if (this.formMedicos.valid) {
      this.prestamosService
        .getFindMedico(
          String(this.formMedicos.value.paterno),
          String(this.formMedicos.value.nombre)
        )
        .subscribe({
          next: (val: any) => {
            if (val.ok) {
              this.medicos = val.medico;
              this.allMedicos = 10;
              this.cargando = true;
              this.isClicked = false;
            } else {
              this.alert.sweet('Busqueda de Médicos', val.msg, 'error');
              this.isClicked = true;
            }
          },
        });
    }
  }

  selectMedico(item: any) {
    this.medico.emit(item);
    this.cargando = false;
    this.formMedicos.reset();
    $('#modal-sel-medico').modal('hide');
  }

  numRegistro() {
    this.cargando = false;
    this.pagination = 1;
    this.findMedicos();
  }

  renderPage(event: number) {
    this.cargando = false;
    this.pagination = event;
    this.findMedicos();
  }

  cleaning() {
    this.cargando = false;
    this.isClicked = false;
    this.formMedicos.reset();
  }
}
