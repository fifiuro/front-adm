import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelecMedicoComponent } from './selec-medico.component';

describe('SelecMedicoComponent', () => {
  let component: SelecMedicoComponent;
  let fixture: ComponentFixture<SelecMedicoComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SelecMedicoComponent]
    });
    fixture = TestBed.createComponent(SelecMedicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
