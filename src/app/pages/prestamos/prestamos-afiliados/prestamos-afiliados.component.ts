import { Component } from '@angular/core';
import { PersonaUnidadService } from 'src/app/services/personaUnidad/persona-unidad.service';
import { ValidacionService } from 'src/app/helpers/validacion/validacion.service';
import { PermisosService } from 'src/app/services/permisos/permisos.service';

@Component({
  selector: 'app-prestamos-afiliados',
  templateUrl: './prestamos-afiliados.component.html',
  styleUrls: ['./prestamos-afiliados.component.css'],
})
export class PrestamosAfiliadosComponent {
  buscar = { matricula: '', ci: '' };
  cargando: boolean = false;
  perUni: any = [];
  pagination: number = 1;
  cantReg: number = 10;
  allPerUni: number = 0;
  public hasError: boolean = false;

  constructor(
    private personaUnidadService: PersonaUnidadService,
    private readonly validacionService: ValidacionService,
    private readonly permisosService: PermisosService
  ) {}

  ngOnInit() {
    this.listPersonaUnidad();
  }

  verificar(compara: string) {
    return this.permisosService.verificarPermisos(compara);
  }

  listPersonaUnidad() {
    this.personaUnidadService
      .getPersonalUnidad(this.pagination, this.cantReg)
      .subscribe({
        next: (res: any) => {
          this.perUni = res.data;
          this.allPerUni = res.count as number;
          this.cargando = true;
        },
      });
  }

  findPersonaUnidad() {
    this.cargando = false;
    if (this.buscar.matricula || this.buscar.ci) {
      this.personaUnidadService
        .getLikePersonaUnidad(this.pagination, this.cantReg, this.buscar)
        .subscribe({
          next: (res: any) => {
            this.perUni = res.data;
            this.allPerUni = res.count;
            this.pagination = 1;
            this.cargando = true;
          },
        });
    } else {
      this.listPersonaUnidad();
    }
  }

  activeSubirArchivos(id: AnalyserNode, activo: Boolean) {}

  numRegistro() {
    this.cargando = false;
    this.pagination = 1;
    // this.listPersonaUnidad();
  }

  renderPage(event: any) {
    this.cargando = false;
    this.pagination = event;
    // this.listPersonaUnidad();
  }

  validacionMatricula() {
    this.buscar.matricula = this.validacionService.validacionMatricula(
      this.buscar.matricula
    );
  }
}
