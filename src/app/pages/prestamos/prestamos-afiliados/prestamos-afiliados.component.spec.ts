import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrestamosAfiliadosComponent } from './prestamos-afiliados.component';

describe('PrestamosAfiliadosComponent', () => {
  let component: PrestamosAfiliadosComponent;
  let fixture: ComponentFixture<PrestamosAfiliadosComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PrestamosAfiliadosComponent]
    });
    fixture = TestBed.createComponent(PrestamosAfiliadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
