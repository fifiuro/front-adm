import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PrestamosComponent } from './prestamos.component';
import { PrestamosListComponent } from './prestamos-list/prestamos-list.component';
import { PrestamosFormComponent } from './prestamos-form/prestamos-form.component';
import { PrestamosAfiliadosComponent } from './prestamos-afiliados/prestamos-afiliados.component';

const routes: Routes = [
  {
    path: '',
    component: PrestamosComponent,
    children: [
      { path: '', redirectTo: 'prestamos-list', pathMatch: 'full' },
      { path: 'prestamos-list', component: PrestamosListComponent },
      {
        path: 'prestamos-form/:id/:idPer/:isEdit',
        component: PrestamosFormComponent,
      },
      { path: 'prestamos-afiliados', component: PrestamosAfiliadosComponent },
      //   { path: 'tipo-atencion-detail/:userId', component: UsersDetailComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PrestamosRoutingModule {}
