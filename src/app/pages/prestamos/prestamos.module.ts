import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrestamosListComponent } from './prestamos-list/prestamos-list.component';
import { PrestamosComponent } from './prestamos.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { PrestamosRoutingModule } from './prestamos-routing.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { PrestamosFormComponent } from './prestamos-form/prestamos-form.component';
import { SelecMedicoComponent } from './selec-medico/selec-medico.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { PrestamosAfiliadosComponent } from './prestamos-afiliados/prestamos-afiliados.component';

@NgModule({
  declarations: [
    PrestamosListComponent,
    PrestamosComponent,
    PrestamosFormComponent,
    SelecMedicoComponent,
    PrestamosAfiliadosComponent,
  ],
  imports: [
    SharedModule,
    CommonModule,
    PrestamosRoutingModule,
    NgxPaginationModule,
    NgSelectModule,
  ],
})
export class PrestamosModule {}
