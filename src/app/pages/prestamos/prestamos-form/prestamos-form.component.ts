import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertsService } from 'src/app/helpers/alerts/alerts.service';
import { TomoInterface } from 'src/app/interfaces';
import { MedicosService } from 'src/app/services/medicos/medicos.service';
import { PersonaUnidadService } from 'src/app/services/personaUnidad/persona-unidad.service';
import { PersonasService } from 'src/app/services/personas/personas.service';
import { PrestamosService } from 'src/app/services/prestamos/prestamos.service';
import { TomoService } from 'src/app/services/subir-archivos/tomo/tomo.service';

@Component({
  selector: 'app-prestamos-form',
  templateUrl: './prestamos-form.component.html',
  styleUrls: ['./prestamos-form.component.css'],
})
export class PrestamosFormComponent {
  idPu!: number;
  idPer!: number;
  idMed!: number;
  isEdit!: boolean;
  header: any;
  tomos!: TomoInterface[];
  selectedTom: any;
  show: boolean = false;
  showMedico: boolean = false;
  selMedico: any;
  idMedico: any;
  data: any = {
    nombres: '',
    apPaterno: '',
    apMaterno: '',
    nombreCompleto: '',
    especialidad: '',
  };
  isClicked: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private readonly personaUnidadService: PersonaUnidadService,
    private readonly tomoService: TomoService,
    private readonly medicoService: MedicosService,
    private readonly prestamoService: PrestamosService,
    private readonly personasService: PersonasService,
    private alert: AlertsService,
    private router: Router
  ) {}

  ngOnInit() {
    this.idPu = parseInt(this.route.snapshot.paramMap.get('id')!);
    this.idPer = parseInt(this.route.snapshot.paramMap.get('idPer')!);
    this.isEdit = this.route.snapshot.paramMap.get('isEdit') == 'true';
    if (this.isEdit) {
      this.loadDataPrestamo();
    }
    this.loadDataPersonaUnidad();
    this.loadDataTomo();
  }

  get fechaPrestamo() {
    return this.formPrestamo.get('fechaPrestamo') as FormControl;
  }

  get horaPrestamo() {
    return this.formPrestamo.get('horaPrestamo') as FormControl;
  }

  get obsPrestamo() {
    return this.formPrestamo.get('obsPrestamo') as FormControl;
  }

  get persona() {
    return this.formPrestamo.get('persona') as FormControl;
  }

  get tomo() {
    return this.formPrestamo.get('tomo') as FormControl;
  }

  get medico() {
    return this.formPrestamo.get('medico') as FormControl;
  }

  get estado() {
    return this.formPrestamo.get('estado') as FormControl;
  }

  formPrestamo = new FormGroup({
    fechaPrestamo: new FormControl('', Validators.required),
    horaPrestamo: new FormControl('', Validators.required),
    obsPrestamo: new FormControl('', Validators.required),
    persona: new FormControl(1, Validators.required),
    tomo: new FormControl('', Validators.required),
    medico: new FormControl(1, Validators.required),
    estado: new FormControl('PRESTAMO', Validators.required),
  });

  receiveMedico($event: any) {
    this.selMedico = $event;
    this.showMedico = true;
    this.medicoService
      .getVerificaMedico(
        this.selMedico.MEDI_NOM,
        this.selMedico.MEDI_APAT,
        this.selMedico.MEDI_AMAT,
        this.selMedico.ESP_NOM
      )
      .subscribe({
        next: (val: any) => {
          if (val) {
            this.idMedico = val[0].idMed;
          }
        },
      });
  }

  matchData() {
    this.data.nombres = this.selMedico.MEDI_NOM;
    this.data.apPaterno = this.selMedico.MEDI_APAT;
    this.data.apMaterno = this.selMedico.MEDI_AMAT;
    this.data.nombreCompleto =
      this.selMedico.MEDI_NOM +
      ' ' +
      this.selMedico.MEDI_APAT +
      ' ' +
      this.selMedico.MEDI_AMAT;
    this.data.especialidad = this.selMedico.ESP_NOM;
  }

  /**
   * Cargar los datos Persona y Unidad
   */
  loadDataPersonaUnidad() {
    if (this.isEdit) {
    } else {
      this.personaUnidadService.getByIdPersonaUnidad(this.idPu).subscribe({
        next: (val: any) => {
          if (val) {
            this.header = val;
            this.show = true;
          }
        },
      });
    }
  }

  /**
   * Cargar los datos Tomos segun la
   * Persona
   */
  loadDataTomo() {
    this.tomoService.getComboTomo(this.idPer).subscribe({
      next: (val: any) => {
        if (val) {
          this.tomos = val;
        } else {
          console.log('No se encontraron Datos.');
        }
      },
    });
  }

  loadDataPrestamo() {
    let medico: any = {
      medico: {
        MEDI_NOM: '',
        MEDI_APAT: '',
        MEDI_AMAT: '',
        ESP_NOM: '',
      },
    };
    let persona: any = {
      persona: {
        nombres: '',
      },
    };
    this.prestamoService.getFindByIdPrestamo(this.idPu).subscribe({
      next: (val: any) => {
        if (val) {
          this.formPrestamo.patchValue(val);
          this.selectedTom = val.tomo.idTom;
          this.idMedico = val.medico.idMed;
          medico.MEDI_NOM = val.medico.nombres;
          medico.MEDI_APAT = val.medico.apPaterno;
          medico.MEDI_AMAT = val.medico.apMaterno;
          medico.ESP_NOM = val.medico.especialidad;
          this.selMedico = medico;
          this.showMedico = true;

          persona.persona.nombres = val.persona.nombres;
          persona.persona.apPaterno = val.persona.apPaterno;
          persona.persona.apMaterno = val.persona.apMaterno;
          persona.persona.matriculaBeneficiario =
            val.persona.matriculaBeneficiario;
          persona.persona.ciTitular = val.persona.ciTitular;
          persona.persona.expedido = val.persona.expedido;
          persona.persona.codigoDescripcion = val.persona.codigoDescripcion;
          persona.persona.estadoAfiliacion = val.persona.estadoAfiliacion;
          persona.persona.empresa = val.persona.empresa;
          persona.unidad = '';
          this.header = persona;
          this.show = true;
        }
      },
    });
  }

  addPrestamo() {
    let idMedico;
    if (this.isEdit) {
      this.editPrestamo();
    } else {
      if (this.selMedico && this.idPu) {
        if (this.formPrestamo.valid) {
          this.matchData();
          if (this.idMedico) {
            idMedico = this.idMedico;
            this.formPrestamo.value.medico = idMedico;
            this.personaUnidadService
              .getByIdPersonaUnidad(this.idPu)
              .subscribe({
                next: (val: any) => {
                  if (val) {
                    this.formPrestamo.value.persona = val.persona.idPer;
                    this.prestamoService
                      .postAddPrestamo(this.formPrestamo.value)
                      .subscribe({
                        next: (val: any) => {
                          if (val) {
                            this.alert.sweet(
                              'Registro de Prestamo',
                              'Los datos de Prestamo se registraron correctamente.',
                              'success'
                            );
                            this.limpiar();
                          } else {
                            this.alert.sweet(
                              'Registro de Prestamo',
                              'Los datos de Prestamo No se registraron.',
                              'error'
                            );
                          }
                        },
                      });
                  } else {
                    this.alert.sweet(
                      'Registro de Prestamo',
                      'Los datos de Prestamo No se registraron.',
                      'error'
                    );
                  }
                },
              });
          } else {
            this.medicoService.postAddMedico(this.data).subscribe({
              next: (val: any) => {
                if (val) {
                  idMedico = val.idMed;
                  this.formPrestamo.value.medico = idMedico;
                  this.personaUnidadService
                    .getByIdPersonaUnidad(this.idPu)
                    .subscribe({
                      next: (val: any) => {
                        if (val) {
                          this.formPrestamo.value.persona = val.persona.idPer;
                          this.prestamoService
                            .postAddPrestamo(this.formPrestamo.value)
                            .subscribe({
                              next: (val: any) => {
                                if (val) {
                                  this.alert.sweet(
                                    'Registro de Prestamo',
                                    'Los datos de Prestamo se registraron correctamente.',
                                    'success'
                                  );
                                  this.limpiar();
                                } else {
                                  this.alert.sweet(
                                    'Registro de Prestamo',
                                    'Los datos de Prestamo No se registraron.',
                                    'error'
                                  );
                                }
                              },
                            });
                        } else {
                          this.alert.sweet(
                            'Registro de Prestamo',
                            'Los datos de Prestamo No se registraron.',
                            'error'
                          );
                        }
                      },
                    });
                } else {
                  this.alert.sweet(
                    'Registro de Prestamo',
                    'Los datos de Prestamo No se registraron.',
                    'error'
                  );
                }
              },
            });
          }
        }
      }
    }
  }

  editPrestamo() {
    if (this.formPrestamo.valid) {
      this.formPrestamo.value.persona = this.idPer;
      this.formPrestamo.value.medico = this.idMedico;
      this.prestamoService
        .putUpdatePrestamo(this.idPu, this.formPrestamo.value)
        .subscribe({
          next: (val: any) => {
            if (val) {
              this.alert.sweet(
                'Edicón de Prestamo',
                'Los datos de Prestamo se modificaron correctamente.',
                'success'
              );
              this.limpiar();
            } else {
              this.alert.sweet(
                'Edición de Prestamo',
                'Los datos de Prestamo no se modificaron.',
                'error'
              );
              this.limpiar();
            }
          },
        });
    }
  }

  searchAndCreateMedico($event: any) {
    this.selMedico = $event;
    this.showMedico = true;
    this.medicoService
      .getVerificaMedico(
        this.selMedico.MEDI_NOM,
        this.selMedico.MEDI_APAT,
        this.selMedico.MEDI_AMAT,
        this.selMedico.ESP_NOM
      )
      .subscribe({
        next: (val: any) => {
          if (val) {
            this.idMedico = val[0].idMed;
          } else {
            this.matchData();
            this.medicoService.postAddMedico(this.data).subscribe({
              next: (val: any) => {
                if (val) {
                  this.idMedico = val.idMed;
                }
              },
            });
          }
        },
      });
  }

  limpiar() {
    this.formPrestamo.reset();
    this.formPrestamo.value.persona = 1;
    this.formPrestamo.value.medico = 1;
    this.formPrestamo.value.estado = 'PRESTAMO';
    this.isClicked = false;
    if (this.isEdit) {
      this.router.navigate(['/pages/prestamos/prestamos-list']);
    } else {
      this.router.navigate(['/pages/subir-archivos/subir-archivos-list']);
    }
  }
}
