import { Component } from '@angular/core';
import { AlertsService } from 'src/app/helpers/alerts/alerts.service';
import { PermisosService } from 'src/app/services/permisos/permisos.service';
import { PrestamosService } from 'src/app/services/prestamos/prestamos.service';

@Component({
  selector: 'app-prestamos-list',
  templateUrl: './prestamos-list.component.html',
  styleUrls: ['./prestamos-list.component.css'],
})
export class PrestamosListComponent {
  buscar = { afialiado: '', medico: '', estado: '' };
  estado = { activo: true };
  tipoEstado: any;
  devolver = {
    fechaDevolucion: '',
    horaDevolucion: '',
    obsDevolucion: '',
    estado: '',
  };
  cargando = false;
  prestamos: any;
  pagination: number = 1;
  cantReg: number = 10;
  allPrestamos: number = 0;
  public hasError: boolean = false;

  constructor(
    private readonly prestamosService: PrestamosService,
    private readonly permisosService: PermisosService,
    public alert: AlertsService
  ) {}

  ngOnInit() {
    this.tipoEstado = [
      {
        estado: 'DEVUELTO',
      },
      {
        estado: 'PRESTAMO',
      },
    ];
    this.listPrestamos();
  }

  verificar(compara: string) {
    return this.permisosService.verificarPermisos(compara);
  }

  listPrestamos() {
    this.prestamosService
      .getPrestamos(this.pagination, this.cantReg)
      .subscribe({
        next: (res: any) => {
          this.prestamos = res.data;
          this.allPrestamos = res.count as number;
          this.cargando = true;
        },
      });
  }

  findPrestamo() {
    if (this.buscar.afialiado || this.buscar.medico || this.buscar.estado) {
      this.cargando = false;
      this.prestamosService
        .getFindPrestamosX3(this.pagination, this.cantReg, this.buscar)
        .subscribe({
          next: (res: any) => {
            this.prestamos = res.data;
            this.allPrestamos = res.count as number;
            this.cargando = true;
          },
        });
    } else {
      this.listPrestamos();
    }
  }

  findPrestamoByID(id: number, item: any) {}

  async devolverPrestamo(id: number, estado: string) {
    var obs: string = '';
    var fecha = new Date();
    var mes = String(fecha.getMonth() + 1);
    if (mes.length == 1) {
      mes = 0 + mes;
    }
    this.prestamosService.fecha().subscribe({
      next: async (val: any) => {
        if (val) {
          this.devolver.fechaDevolucion = val.fecha;
          this.devolver.horaDevolucion = val.hora;
          this.devolver.estado = 'DEVUELTO';
          obs = await this.alert.sweetInput(
            'Devoluciónde Prestamo',
            'Escriba observación de la devolución',
            ''
          );
          if (obs) {
            this.devolver.obsDevolucion = obs;
          } else {
            this.devolver.obsDevolucion = 'NN';
          }
          this.prestamosService.putUpdatePrestamo(id, this.devolver).subscribe({
            next: (val: any) => {
              if (val.affected === 1) {
                this.alert.sweet(
                  'Edición de Prestamo',
                  'Se cambio el estado de Prestamo correctamente.',
                  'success'
                );
                this.listPrestamos();
              } else {
                this.alert.sweet(
                  'Edición de Prestamo',
                  'No se cambio el estado de Prestamo.',
                  'error'
                );
              }
            },
          });
        } else {
          this.alert.sweet(
            'Edición de Prestamo',
            'No se pudo recuperar las fechas y horas del servidor.',
            'error'
          );
        }
      },
    });
  }

  activePrestamo(id: number, activo: boolean) {
    this.estado.activo = !activo;
    this.prestamosService.putUpdatePrestamo(id, this.estado).subscribe({
      next: (val: any) => {
        if (val.affected === 1) {
          this.alert.sweet(
            'Edición de Prestamo',
            'Se cambio la situación de actividad correctamente.',
            'success'
          );
          this.listPrestamos();
        } else {
          this.alert.sweet(
            'Edición de Prestamo',
            'No se cambio la situación de actividad correctamente.',
            'error'
          );
        }
      },
    });
  }

  numRegistro() {
    this.cargando = false;
    this.pagination = 1;
    this.listPrestamos();
  }

  renderPage(event: number) {
    this.cargando = false;
    this.pagination = event;
    this.listPrestamos();
  }
}
