import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UnidadesComponent } from './unidades.component';
import { UnidadesListComponent } from './unidades-list/unidades-list.component';
import { UnidadesFormComponent } from './unidades-form/unidades-form.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { UnidadesRoutingModule } from './unidades-routing.module';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [
    UnidadesComponent,
    UnidadesListComponent,
    UnidadesFormComponent,
  ],
  imports: [
    SharedModule,
    CommonModule,
    UnidadesRoutingModule,
    NgxPaginationModule,
  ],
})
export class UnidadesModule {}
