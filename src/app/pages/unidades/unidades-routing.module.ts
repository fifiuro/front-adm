import { RouterModule, Routes } from '@angular/router';
import { UnidadesComponent } from './unidades.component';
import { NgModule } from '@angular/core';
import { UnidadesListComponent } from './unidades-list/unidades-list.component';

const routes: Routes = [
  {
    path: '',
    component: UnidadesComponent,
    children: [
      { path: '', redirectTo: 'unidades-list', pathMatch: 'full' },
      { path: 'unidades-list', component: UnidadesListComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UnidadesRoutingModule {}
