import { Component } from '@angular/core';
import { AlertsService } from 'src/app/helpers/alerts/alerts.service';
import { PermisosService } from 'src/app/services/permisos/permisos.service';
import { UnidadesService } from 'src/app/services/unidades/unidades.service';
declare var $: any;
@Component({
  selector: 'app-unidades-list',
  templateUrl: './unidades-list.component.html',
  styleUrls: ['./unidades-list.component.css'],
})
export class UnidadesListComponent {
  buscar = { unidad: '' };
  estado = { activo: true };
  cargando = false;
  unidad!: any[];
  pagination: number = 1;
  cantReg: number = 10;
  allUnidad: number = 0;
  public hasError: boolean = false;

  titulo = '';
  isEdit: boolean = false;
  item: any;

  constructor(
    public unidadService: UnidadesService,
    private readonly permisosService: PermisosService,
    public alert: AlertsService
  ) {}

  ngOnInit() {
    this.listUnidad();
  }

  verificar(compara: string) {
    return this.permisosService.verificarPermisos(compara);
  }

  ngDoCheck() {
    if (this.unidadService.refresh) {
      this.numRegistro();
      this.unidadService.refresh = false;
    }
  }

  listUnidad() {
    this.unidadService.getUnidad(this.pagination, this.cantReg).subscribe({
      next: (res: any) => {
        this.unidad = res.data;
        this.allUnidad = res.count as number;
        this.cargando = true;
      },
    });
  }

  findUnidad() {
    this.cargando = false;
    if (this.buscar.unidad) {
      this.unidadService
        .getLikeUnidad(this.pagination, this.cantReg, this.buscar.unidad)
        .subscribe({
          next: (res: any) => {
            this.unidad = res.data;
            this.allUnidad = res.count;
            this.pagination = 1;
            this.cargando = true;
          },
        });
    } else {
      this.listUnidad();
    }
  }

  findUnidadByID(item: any) {
    this.isEdit = true;
    this.item = item;
    $('#modal-add-edit').modal('show');
  }

  activeUnidad(id: number, activo: any) {
    this.estado.activo = !activo;
    this.unidadService.putUpdateUnidad(id, this.estado).subscribe({
      next: (val: any) => {
        if (val.affected === 1) {
          this.alert.sweet(
            'Edición de Unidad',
            'Se cambio de estado correctamente.',
            'success'
          );
          this.listUnidad();
        } else {
          this.alert.sweet(
            'Edición de Unidad',
            'No se pudo realizar el cambio de estado.',
            'error'
          );
        }
      },
    });
  }

  numRegistro() {
    this.cargando = false;
    this.pagination = 1;
    this.listUnidad();
  }

  renderPage(event: number) {
    this.cargando = false;
    this.pagination = event;
    this.listUnidad();
  }

  formAddUnidad(titulo: boolean) {
    this.isEdit = false;
    $('#modal-add-edit').modal('show');
  }

  limpiar(e: boolean) {
    this.item = '';
  }
}
