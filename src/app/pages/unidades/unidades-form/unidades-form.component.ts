import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AlertsService } from 'src/app/helpers/alerts/alerts.service';
import { UnidadesService } from 'src/app/services/unidades/unidades.service';
declare var $: any;

@Component({
  selector: 'app-unidades-form',
  templateUrl: './unidades-form.component.html',
  styleUrls: ['./unidades-form.component.css'],
})
export class UnidadesFormComponent {
  @Input() titulo!: string;
  @Input() isEdit!: boolean;
  @Input() item: any;
  @Output() clearItem = new EventEmitter<boolean>();
  isClicked: boolean = false;
  router: any;

  constructor(
    public unidadService: UnidadesService,
    private alert: AlertsService
  ) {}

  ngOnChanges() {
    this.recoveryUnidad();
  }

  get unidad() {
    return this.formUnidad.get('unidad') as FormControl;
  }

  get sigla() {
    return this.formUnidad.get('sigla') as FormControl;
  }

  formUnidad = new FormGroup({
    unidad: new FormControl('', Validators.required),
    sigla: new FormControl('', Validators.required),
  });

  createUnidad() {
    this.isClicked = true;
    if (this.formUnidad.valid) {
      if (this.isEdit) {
        this.unidadService
          .putUpdateUnidad(this.item.idUni, this.formUnidad.value)
          .subscribe({
            next: (val: any) => {
              if (val.affected === 1) {
                this.alert.sweet(
                  'Edición de Unidad',
                  'Se realizó correctamente.',
                  'success'
                );
                this.limpiar();
                this.unidadService.refresh = true;
                this.isClicked = false;
                $('#modal-add-edit').modal('hide');
              } else {
                this.alert.sweet(
                  'Edición Unidad',
                  'No se pudo realizar la edición.',
                  'error'
                );
                this.limpiar();
                this.unidadService.refresh = true;
                this.isClicked = false;
                $('#modal-add-edit').modal('hide');
              }
            },
          });
      } else {
        this.unidadService.postAddUnidad(this.formUnidad.value).subscribe({
          next: (val: any) => {
            if (val) {
              this.alert.sweet(
                'Registro de Unidad',
                'Se realizó correctamente.',
                'success'
              );
              this.limpiar();
              this.unidadService.refresh = true;
            } else {
              this.alert.sweet(
                'Registro de Unidad',
                'No se pudo realizar el registro.',
                'error'
              );
              this.limpiar();
              this.unidadService.refresh = true;
            }
          },
        });
        this.isClicked = false;
        $('#modal-add-edit').modal('hide');
      }
    }
  }

  recoveryUnidad() {
    if (this.item) {
      this.unidadService.getByIDUnidad(this.item.idUni).subscribe({
        next: (val: any) => {
          this.formUnidad.patchValue(val);
        },
      });
    }
  }

  limpiar() {
    this.formUnidad.reset();
    this.item = '';
    this.isClicked = false;
    this.clearItem.emit(true);
  }
}
