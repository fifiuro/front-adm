import { Component } from '@angular/core';
import { ValidacionService } from 'src/app/helpers/validacion/validacion.service';
import { PermisosService } from 'src/app/services/permisos/permisos.service';
import { PersonaUnidadService } from 'src/app/services/personaUnidad/persona-unidad.service';

@Component({
  selector: 'app-buscar-archivos-afiliados',
  templateUrl: './buscar-archivos-afiliados.component.html',
  styleUrls: ['./buscar-archivos-afiliados.component.css'],
})
export class BuscarArchivosAfiliadosComponent {
  buscar = { matricula: '', ci: '' };
  cargando: boolean = false;
  perUni: any = [];
  pagination: number = 1;
  cantReg: number = 10;
  allPerUni: number = 0;
  public hasError: boolean = false;

  constructor(
    private readonly personaUnidadService: PersonaUnidadService,
    private readonly validacionService: ValidacionService,
    private readonly permisosService: PermisosService,
  ) {}

  ngOnInit() {
    this.listPersonaUnidad();
  }

  verificar(compara: string) {
    return this.permisosService.verificarPermisos(compara);
  }

  listPersonaUnidad() {
    this.personaUnidadService
      .getPersonalUnidad(this.pagination, this.cantReg)
      .subscribe({
        next: (res: any) => {
          this.perUni = res.data;
          this.allPerUni = res.count as number;
          this.cargando = true;
        },
      });
  }

  findPersonaUnidad() {
    this.cargando = false;
    if (this.buscar.matricula || this.buscar.ci) {
      this.personaUnidadService
        .getLikePersonaUnidad(this.pagination, this.cantReg, this.buscar)
        .subscribe({
          next: (res: any) => {
            this.perUni = res.data;
            this.allPerUni = res.count;
            this.pagination = 1;
            this.cargando = true;
          },
        });
    } else {
      this.listPersonaUnidad();
    }
  }

  numRegistro() {
    this.cargando = false;
    this.pagination = 1;
  }

  renderPage(event: any) {
    this.cargando = false;
    this.pagination = event;
  }

  validacionMatricula() {
    this.buscar.matricula = this.validacionService.validacionMatricula(
      this.buscar.matricula
    );
  }
}
