import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuscarArchivosAfiliadosComponent } from './buscar-archivos-afiliados.component';

describe('BuscarArchivosAfiliadosComponent', () => {
  let component: BuscarArchivosAfiliadosComponent;
  let fixture: ComponentFixture<BuscarArchivosAfiliadosComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BuscarArchivosAfiliadosComponent]
    });
    fixture = TestBed.createComponent(BuscarArchivosAfiliadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
