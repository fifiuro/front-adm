import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BuscarArchivosComponent } from './buscar-archivos.component';
import { BuscarArchivosAfiliadosComponent } from './buscar-archivos-afiliados/buscar-archivos-afiliados.component';

const routes: Routes = [
  {
    path: '',
    component: BuscarArchivosComponent,
    children: [
      { path: '', redirectTo: 'buscar-archivos-afiliados', pathMatch: 'full' },
      {
        path: 'buscar-archivos-afiliados',
        component: BuscarArchivosAfiliadosComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BuscarArchivosRoutingModule {}
