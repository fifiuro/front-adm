import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { BuscarArchivosRoutingModule } from './buscar-archivos-routing.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgSelectModule } from '@ng-select/ng-select';
import { BuscarArchivosComponent } from './buscar-archivos.component';
import { BuscarArchivosAfiliadosComponent } from './buscar-archivos-afiliados/buscar-archivos-afiliados.component';

@NgModule({
  declarations: [BuscarArchivosComponent, BuscarArchivosAfiliadosComponent],
  imports: [
    SharedModule,
    CommonModule,
    BuscarArchivosRoutingModule,
    NgxPaginationModule,
    NgSelectModule,
  ],
})
export class BuscarArchivosModule {}
