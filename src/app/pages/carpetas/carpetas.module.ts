import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarpetasComponent } from './carpetas.component';
import { CarpetasListComponent } from './carpetas-list/carpetas-list.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { CarpetasRoutingModule } from './carpetas-routing.module';
import { NgxPaginationModule } from 'ngx-pagination';

import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { CarpetasEstructuraComponent } from './carpetas-estructura/carpetas-estructura.component';
import { CarpetasVerComponent } from './carpetas-ver/carpetas-ver.component';

@NgModule({
  declarations: [
    CarpetasComponent,
    CarpetasListComponent,
    CarpetasEstructuraComponent,
    CarpetasVerComponent,
  ],
  imports: [
    SharedModule,
    CommonModule,
    CarpetasRoutingModule,
    NgxPaginationModule,
    NgSelectModule,
    FormsModule,
  ],
})
export class CarpetasModule {}
