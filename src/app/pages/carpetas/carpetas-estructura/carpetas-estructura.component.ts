import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { MenuEditor } from '@davicotico/menu-editor';
import { AlertsService } from 'src/app/helpers/alerts/alerts.service';
import { TipoAtencionService } from 'src/app/services';
import { CarpetasService } from 'src/app/services/carpetas/carpetas.service';
import { FtpService } from 'src/app/services/subir-archivos/ftp/ftp.service';
import { UnidadesService } from 'src/app/services/unidades/unidades.service';

@Component({
  selector: 'app-carpetas-estructura',
  templateUrl: './carpetas-estructura.component.html',
  styleUrls: ['./carpetas-estructura.component.css'],
})
export class CarpetasEstructuraComponent {
  menuEditor!: any;
  NewFolder = [
    {
      text: 'PRINCIPAL',
      href: '/',
      tooltip: 'principal',
      icon: 'fas fa-folder',
      children: [],
    },
  ];
  newItem = {
    text: '',
    href: '/',
    tooltip: 'agregado',
    icon: 'fas fa-folder',
    children: [],
  };
  EditFolder = [];

  isEdit!: boolean;
  item!: number;
  unidades: any;
  tipoAtencion: any;
  isClicked: boolean = false;
  selectedCar!: any;
  selectedRuta!: any;
  editRuta!: string;

  constructor(
    private alert: AlertsService,
    private unidadService: UnidadesService,
    private rutaActiva: ActivatedRoute,
    private router: Router,
    private carpetaService: CarpetasService,
    private tipoAtencionService: TipoAtencionService,
    private ftpService: FtpService
  ) {}

  ngOnInit() {
    this.unidadService.getComboUnidad().subscribe({
      next: (val: any) => {
        this.unidades = val;
      },
    });
    this.isEdit = this.rutaActiva.snapshot.params['isEdit'] == 'true';
    this.item = this.rutaActiva.snapshot.params['item'];
    if (this.isEdit) {
      this.recoveryCarpeta();
    }
  }

  get unidad() {
    return this.formCarpeta.get('unidad') as FormControl;
  }

  get estructura() {
    return this.formCarpeta.get('estructura') as FormControl;
  }

  get ruta() {
    return this.formCarpeta.get('ruta') as FormControl;
  }

  formCarpeta = new FormGroup({
    unidad: new FormControl('', Validators.required),
    estructura: new FormControl('H', Validators.required),
    ruta: new FormControl('', Validators.required),
  });

  loadStart(carpeta: string) {
    this.menuEditor = new MenuEditor('element-id', { maxLevel: 3 });
    if (carpeta) {
      this.menuEditor.setArray(JSON.parse(carpeta));
    } else {
      this.menuEditor.setArray(this.NewFolder);
    }
    // Funcion para Editar las Carpetas
    this.menuEditor.onClickEdit((event: any) => {
      let itemData = event.item.getDataset();
      this.menuEditor.edit(event.item);
      this.editFolder(itemData.text);
    });
    // Fin
    // Funcion para Eliminar las Carpetas
    this.menuEditor.onClickDelete((event: any) => {
      this.alert.sweetConfirm(
        '¿Esta seguro?',
        'De eliminar la Carpeta!',
        'Si, Eliminar!',
        'No, Cancelar!',
        'La Carpeta ha sido eliminado.',
        'Salvaste a tu Carpeta!',
        event
      );
    });
    // Fin
    this.menuEditor.mount();
  }

  loadNew() {
    this.menuEditor = new MenuEditor('element-id', { maxLevel: 2 });
    this.menuEditor.setArray(this.NewFolder);
    // Funcion para Editar las Carpetas
    this.menuEditor.onClickEdit((event: any) => {
      let itemData = event.item.getDataset();
      this.menuEditor.edit(event.item);
      this.editFolder(itemData.text);
    });
    // Fin
    // Funcion para Eliminar las Carpetas
    this.menuEditor.onClickDelete((event: any) => {
      this.alert.sweetConfirm(
        '¿Esta seguro?',
        'De eliminar la Carpeta!',
        'Si, Eliminar!',
        'No, Cancelar!',
        'La Carpeta ha sido eliminado.',
        'Salvaste a tu Carpeta!',
        event
      );
    });
    // Fin
    this.menuEditor.mount();
    this.tipoAtencionService.getComboTipoAtencion().subscribe({
      next: (val: any) => {
        if (val.length > 0) {
          this.tipoAtencion = val;
          this.tipoAtencion.forEach((element: any) => {
            this.newItem = {
              text: element.tipo,
              href: '/',
              tooltip: 'agregado',
              icon: 'fas fa-folder',
              children: [],
            };
            this.menuEditor.add(this.newItem);
          });
        }
      },
    });
  }

  loadRecovery(carpeta: string) {}

  async addFolder() {
    let nameFolder = await this.alert.sweetInput(
      'Ingrese el nombre de la Carpeta:',
      'Nombre de la Carpeta',
      ''
    );
    if (nameFolder) {
      this.newItem.text = nameFolder;
      this.menuEditor.add(this.newItem);
    } else {
      this.alert.sweet(
        'Agregar Carpeta',
        'El nombre de la Carpeta no debe ser vacio',
        'error'
      );
    }
  }

  async editFolder(nombre: string) {
    let nameFolder = await this.alert.sweetInput(
      'Ingrese el nombre a cambiar: ',
      'Actual: "' + nombre + '", Nuevo nombre',
      ''
    );

    if (nameFolder) {
      let data = {
        text: nameFolder,
        href: '/',
        icon: 'fas fa-folder',
        tooltip: '',
      };
      this.menuEditor.update(data);
    } else {
      this.alert.sweet(
        'Agregar Carpeta',
        'El nombre de la Carpeta no debe ser vacio',
        'error'
      );
    }
  }

  clearFolder() {
    this.menuEditor.empty();
  }

  checkRuta() {
    if (!this.isEdit) {
      if (this.formCarpeta.value.unidad && this.formCarpeta.value.ruta) {
        this.loadStart('');
      }
    }
  }

  changeUnidad() {
    let u = this.unidades.find((x: any) => x.idUni == this.selectedCar);
    if (u) {
      this.selectedRuta = u.unidad.replace(/\s+/g, '');
    } else {
      this.selectedRuta = '';
    }
  }

  async createCarpeta() {
    this.isClicked = true;
    if (this.formCarpeta.valid) {
      if (this.isEdit) {
        this.formCarpeta.value.estructura = JSON.stringify(
          this.menuEditor.getString()
        );
        this.carpetaService
          .putUpdateCarpeta(this.item, this.formCarpeta.value)
          .subscribe({
            next: (val: any) => {
              if (val.affected === 1) {
                this.alert.sweet(
                  'Edición Carpeta',
                  'Se realizó correctamente.',
                  'success'
                );
                if (this.editRuta != this.formCarpeta.value.ruta) {
                  this.renameFolder(
                    this.editRuta,
                    this.formCarpeta.value.ruta!
                  );
                }
                this.limpiar();
                this.isClicked = false;
              }
            },
          });
      } else {
        this.formCarpeta.value.estructura = JSON.stringify(
          this.menuEditor.getString()
        );
        this.carpetaService.postAddCarpeta(this.formCarpeta.value).subscribe({
          next: (val: any) => {
            if (val) {
              this.alert.sweet(
                'Registro de Carpeta',
                'Se realizó correctamente.',
                'success'
              );
              this.limpiar();
              this.isClicked = false;
              this.createFolderPrincipal(val.ruta);
            }
          },
        });
      }
    }
  }

  createFolderPrincipal(carpeta: string) {
    this.ftpService.createStructure('*', carpeta).subscribe({
      next: (val: any) => {
        console.log(val);
      },
    });
  }

  renameFolder(nomActual: string, nomNew: string) {
    this.ftpService.renemaFolder(nomActual, nomNew).subscribe({
      next: (val: any) => {
        if (val.code == 250) {
          this.alert.sweet(
            'Cambio de Nombre de Carpeta',
            'Se realizó correctamente.',
            'success'
          );
        }
      },
    });
  }

  recoveryCarpeta() {
    if (this.item) {
      this.carpetaService.getByIDCarpeta(this.item).subscribe({
        next: (val: any) => {
          this.formCarpeta.patchValue(val);
          this.selectedCar = val.unidad.idUni;
          this.editRuta = val.ruta;
          let carpeta = JSON.parse(val.estructura);
          this.loadStart(carpeta);
        },
      });
    }
  }

  limpiar() {
    this.formCarpeta.reset();
    this.NewFolder = [];
    this.isClicked = false;
    this.router.navigate(['/pages/carpetas/carpetas-list']);
  }
}
