import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarpetasEstructuraComponent } from './carpetas-estructura.component';

describe('CarpetasEstructuraComponent', () => {
  let component: CarpetasEstructuraComponent;
  let fixture: ComponentFixture<CarpetasEstructuraComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CarpetasEstructuraComponent]
    });
    fixture = TestBed.createComponent(CarpetasEstructuraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
