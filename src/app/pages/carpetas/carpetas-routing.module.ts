import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CarpetasListComponent } from './carpetas-list/carpetas-list.component';
import { CarpetasComponent } from './carpetas.component';
import { CarpetasEstructuraComponent } from './carpetas-estructura/carpetas-estructura.component';

const routes: Routes = [
  {
    path: '',
    component: CarpetasComponent,
    children: [
      { path: '', redirectTo: 'carpetas-list', pathMatch: 'full' },
      { path: 'carpetas-list', component: CarpetasListComponent },
      { path: 'carpetas-estructura/:isEdit/:item', component: CarpetasEstructuraComponent},
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CarpetasRoutingModule {}
