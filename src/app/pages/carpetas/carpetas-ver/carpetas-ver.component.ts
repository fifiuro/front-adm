import { Component, Input } from '@angular/core';
import { MenuEditor } from '@davicotico/menu-editor';

@Component({
  selector: 'app-carpetas-ver',
  templateUrl: './carpetas-ver.component.html',
  styleUrls: ['./carpetas-ver.component.css'],
})
export class CarpetasVerComponent {
  @Input() item!: any;
  @Input() estructura: any;
  menuEditor!: any;
  cargando: boolean = false;

  loadStart() {
    this.menuEditor = new MenuEditor('element-id', { maxLevel: 3 });
    this.menuEditor.setArray(JSON.parse(this.estructura));
    this.cargando = true;
    this.menuEditor.mount();
  }

  limpiar() {
    this.menuEditor.empty();
    this.cargando = false;
  }
}
