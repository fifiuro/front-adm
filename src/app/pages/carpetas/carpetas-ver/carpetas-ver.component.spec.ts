import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarpetasVerComponent } from './carpetas-ver.component';

describe('CarpetasVerComponent', () => {
  let component: CarpetasVerComponent;
  let fixture: ComponentFixture<CarpetasVerComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CarpetasVerComponent]
    });
    fixture = TestBed.createComponent(CarpetasVerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
