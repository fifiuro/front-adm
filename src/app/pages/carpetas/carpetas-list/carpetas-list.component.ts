import { Component } from '@angular/core';
import { AlertsService } from 'src/app/helpers/alerts/alerts.service';
import { CarpetasService } from 'src/app/services/carpetas/carpetas.service';
import { PermisosService } from 'src/app/services/permisos/permisos.service';
declare var $: any;

@Component({
  selector: 'app-carpetas-list',
  templateUrl: './carpetas-list.component.html',
  styleUrls: ['./carpetas-list.component.css'],
})
export class CarpetasListComponent {
  buscar = { carpeta: '' };
  estado = { activo: true };
  cargando = false;
  carpetas: any;
  pagination: number = 1;
  cantReg: number = 10;
  allCarpetas: number = 0;
  public hasError: boolean = false;

  titulo = '';
  isEdit: boolean = false;
  item: any;
  estructura!: any;

  constructor(
    public carpetaService: CarpetasService,
    private readonly permisosService: PermisosService,
    public alert: AlertsService
  ) {}

  ngOnInit() {
    this.listCarpeta();
  }

  verificar(compara: string) {
    return this.permisosService.verificarPermisos(compara);
  }

  ngDoCheck() {
    if (this.carpetaService.refresh) {
      this.numRegistro();
      this.carpetaService.refresh = false;
    }
  }

  listCarpeta() {
    this.carpetaService.getCarpetas(this.pagination, this.cantReg).subscribe({
      next: (res: any) => {
        this.carpetas = res.data;
        this.allCarpetas = res.count as number;
        this.cargando = true;
      },
    });
  }

  findCarpeta() {
    this.cargando = false;
    if (this.buscar.carpeta) {
      this.carpetaService
        .getLikeCarpeta(this.pagination, this.cantReg, this.buscar.carpeta)
        .subscribe({
          next: (res: any) => {
            this.carpetas = res.data;
            this.allCarpetas = res.count;
            this.pagination = 1;
            this.cargando = true;
          },
        });
    } else {
      this.listCarpeta();
    }
  }

  activeCarpeta(id: number, activo: any) {
    this.estado.activo = !activo;
    this.carpetaService.putUpdateCarpeta(id, this.estado).subscribe({
      next: (val: any) => {
        if (val.affected === 1) {
          this.alert.sweet(
            'Edición de Carpeta',
            'Se cambio de estado correctamente.',
            'success'
          );
          this.listCarpeta();
        } else {
          this.alert.sweet(
            'Edición de Carpeta',
            'No se pudo realizar el cambio de estado.',
            'error'
          );
        }
      },
    });
  }

  carpetaEstructura(item: any) {
    this.item = item;
    this.estructura = JSON.parse(this.item.estructura);
    $('#modal-estructura').modal('show');
  }

  numRegistro() {
    this.cargando = false;
    this.pagination = 1;
    this.listCarpeta();
  }

  renderPage(event: any) {
    this.cargando = false;
    this.pagination = event;
    this.listCarpeta();
  }

  formAddCarpeta() {
    this.isEdit = false;
    $('#modal-add-edit').modal('show');
  }

  limpiar(e: boolean) {
    this.item = '';
  }
}
