import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarpetasListComponent } from './carpetas-list.component';

describe('CarpetasListComponent', () => {
  let component: CarpetasListComponent;
  let fixture: ComponentFixture<CarpetasListComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CarpetasListComponent]
    });
    fixture = TestBed.createComponent(CarpetasListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
