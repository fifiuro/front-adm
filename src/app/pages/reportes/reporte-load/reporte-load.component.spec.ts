import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteLoadComponent } from './reporte-load.component';

describe('ReporteLoadComponent', () => {
  let component: ReporteLoadComponent;
  let fixture: ComponentFixture<ReporteLoadComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ReporteLoadComponent]
    });
    fixture = TestBed.createComponent(ReporteLoadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
