import { Component } from '@angular/core';
import { PermisosService } from 'src/app/services/permisos/permisos.service';
import { ReportesService } from 'src/app/services/reportes/reportes.service';

@Component({
  selector: 'app-reporte-load',
  templateUrl: './reporte-load.component.html',
  styleUrls: ['./reporte-load.component.css'],
})
export class ReporteLoadComponent {
  buscar = { fInicio: '', fFin: '' };
  cargando: boolean = true;
  reporte: any = [];
  pagination: number = 1;
  cantReg: number = 10;
  allReporte: number = 0;
  public hasError: boolean = false;
  showDownload: boolean = true;

  constructor(
    private readonly reportesService: ReportesService,
    private readonly permisosService: PermisosService
  ) {}

  verificar(compara: string) {
    return this.permisosService.verificarPermisos(compara);
  }

  findReporteLoad() {
    this.cargando = false;
    if (this.buscar.fInicio && this.buscar.fFin) {
      this.reportesService
        .getReporteLoadData(this.buscar.fInicio, this.buscar.fFin)
        .subscribe({
          next: (res: any) => {
            if (res) {
              this.reporte = res;
              this.allReporte = 1;
              this.cargando = true;
              this.showDownload = false;
            }
          },
        });
    }
  }

  downloadReporte() {
    if (this.buscar.fInicio && this.buscar.fFin) {
      this.reportesService
        .getDownloadReporteConteo(this.buscar.fInicio, this.buscar.fFin)
        .subscribe({
          next: (archivo) => {
            if (archivo) {
              const blob = new Blob([archivo], { type: 'application/pdf' });
              const url = window.URL.createObjectURL(blob);
              const enlace = document.createElement('a');
              enlace.href = url;
              enlace.target = '_blank';
              enlace.click();
              window.URL.revokeObjectURL(url);
              this.showDownload = true;
            }
          },
        });
    }
  }

  numRegistro() {
    this.cargando = false;
    this.pagination = 1;
  }

  renderPage(event: any) {
    this.cargando = false;
    this.pagination = event;
  }
}
