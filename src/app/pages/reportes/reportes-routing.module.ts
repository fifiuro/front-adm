import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReportesComponent } from './reportes.component';
import { ReporteLoadComponent } from './reporte-load/reporte-load.component';

const routes: Routes = [
  {
    path: '',
    component: ReportesComponent,
    children: [
      { path: '', redirectTo: 'reportes-load', pathMatch: 'full' },
      { path: 'reportes-load', component: ReporteLoadComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReportesRoutingModule {}
