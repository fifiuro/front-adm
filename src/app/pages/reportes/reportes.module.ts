import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportesComponent } from './reportes.component';
import { ReporteLoadComponent } from './reporte-load/reporte-load.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReportesRoutingModule } from './reportes-routing.module';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [ReportesComponent, ReporteLoadComponent],
  imports: [
    SharedModule,
    CommonModule,
    ReportesRoutingModule,
    NgxPaginationModule,
  ],
})
export class ReportesModule {}
