import { NgModule } from '@angular/core';
import { SharedModule } from '../../core/shared/shared.module';
import { CommonModule } from '@angular/common';
import { UsersComponent } from './users.component';
import { UsersListComponent } from './users-list/users-list.component';
import { UsersDetailComponent } from './users-detail/users-detail.component';
import { UsersRoutingModule } from './users-routing.module';

@NgModule({
  declarations: [UsersComponent, UsersListComponent, UsersDetailComponent],
  imports: [SharedModule, CommonModule, UsersRoutingModule],
})
export class UsersModule {}
