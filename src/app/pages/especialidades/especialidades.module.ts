import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EspecialidadesComponent } from './especialidades.component';
import { EspecialidadesListComponent } from './especialidades-list/especialidades-list.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { EspecialidadesRoutingModule } from './especialidades-routing.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { EspecialidadesFormComponent } from './especialidades-form/especialidades-form.component';

@NgModule({
  declarations: [EspecialidadesComponent, EspecialidadesListComponent, EspecialidadesFormComponent],
  imports: [
    SharedModule,
    CommonModule,
    EspecialidadesRoutingModule,
    NgxPaginationModule,
  ],
})
export class EspecialidadesModule {}
