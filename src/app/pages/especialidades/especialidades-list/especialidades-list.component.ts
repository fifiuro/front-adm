import { Component } from '@angular/core';
import { AlertsService } from 'src/app/helpers/alerts/alerts.service';
import { EspecialidadService } from 'src/app/services/especialidades/especialidad.service';
import { PermisosService } from 'src/app/services/permisos/permisos.service';
declare var $: any;

@Component({
  selector: 'app-especialidades-list',
  templateUrl: './especialidades-list.component.html',
  styleUrls: ['./especialidades-list.component.css'],
})
export class EspecialidadesListComponent {
  buscar = { especialidad: '' };
  estado = { activo: true };
  cargando = false;
  especialidades!: any[];
  pagination: number = 1;
  cantReg: number = 10;
  allEspecialidad: number = 0;
  public hasError: boolean = false;

  titulo = '';
  isEdit: boolean = false;
  item: any;

  constructor(
    private readonly especialidadService: EspecialidadService,
    private readonly permisosService: PermisosService,
    public alert: AlertsService
  ) {}

  ngOnInit() {
    this.listEspecialidad();
  }

  verificar(compara: string) {
    return this.permisosService.verificarPermisos(compara);
  }

  ngDoCheck() {
    if (this.especialidadService.refresh) {
      this.numRegistro();
      this.especialidadService.refresh = false;
    }
  }

  listEspecialidad() {
    this.especialidadService
      .getEspecialidades(this.pagination, this.cantReg)
      .subscribe({
        next: (res: any) => {
          this.especialidades = res.data;
          this.allEspecialidad = res.count as number;
          this.cargando = true;
        },
      });
  }

  findEspecialidad() {
    this.cargando = false;
    if (this.buscar.especialidad) {
      this.especialidadService
        .getLikeExpecialidad(
          this.pagination,
          this.cantReg,
          this.buscar.especialidad
        )
        .subscribe({
          next: (res: any) => {
            this.especialidades = res.data;
            this.allEspecialidad = res.count;
            this.pagination = 1;
            this.cargando = true;
          },
        });
    } else {
      this.listEspecialidad();
    }
  }

  findEspecialidadByID(item: any) {
    this.isEdit = true;
    this.item = item;
    $('#modal-add-edit').modal('show');
  }

  activeEspecialidad(id: number, activo: any) {
    this.estado.activo = !activo;
    this.especialidadService.putUpdateEspecialidad(id, this.estado).subscribe({
      next: (val: any) => {
        if (val.affected === 1) {
          this.alert.sweet(
            'Edición de Especialidad',
            'Se cambio de estado correctamente',
            'success'
          );
          this.listEspecialidad();
        } else {
          this.alert.sweet(
            'Edición de Especialidad',
            'No se pudo realizar el cambio de estado.',
            'error'
          );
        }
      },
    });
  }

  numRegistro() {
    this.cargando = false;
    this.pagination = 1;
    this.listEspecialidad();
  }

  renderPage(event: number) {
    this.cargando = false;
    this.pagination = event;
    this.listEspecialidad();
  }

  formAddEspecialidad(titulo: boolean) {
    this.isEdit = false;
    $('#modal-add-efit').modal('show');
  }

  limpiar(e: boolean) {
    this.item = '';
  }
}
