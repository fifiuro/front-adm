import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EspecialidadesListComponent } from './especialidades-list/especialidades-list.component';
import { EspecialidadesComponent } from './especialidades.component';

const routes: Routes = [
  {
    path: '',
    component: EspecialidadesComponent,
    children: [
      { path: '', redirectTo: 'especialidades-list', pathMatch: 'full' },
      { path: 'especialidades-list', component: EspecialidadesListComponent },
      //   { path: 'tipo-atencion-detail/:userId', component: UsersDetailComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EspecialidadesRoutingModule {}
