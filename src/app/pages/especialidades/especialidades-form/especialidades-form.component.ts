import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AlertsService } from 'src/app/helpers/alerts/alerts.service';
import { EspecialidadService } from 'src/app/services/especialidades/especialidad.service';
declare var $: any;

@Component({
  selector: 'app-especialidades-form',
  templateUrl: './especialidades-form.component.html',
  styleUrls: ['./especialidades-form.component.css'],
})
export class EspecialidadesFormComponent {
  @Input() titulo!: string;
  @Input() isEdit!: boolean;
  @Input() item!: any;
  @Output() clearItem = new EventEmitter<boolean>();
  isClicked: boolean = false;

  constructor(
    public especialidaService: EspecialidadService,
    private alert: AlertsService
  ) {}

  ngOnChanges() {
    this.recoveryEspecialidad();
  }

  get especialidad() {
    return this.formEspecialidad.get('especialidad') as FormControl;
  }

  formEspecialidad = new FormGroup({
    especialidad: new FormControl('', Validators.required),
  });

  createEspecialidad() {
    this.isClicked = true;
    if (this.formEspecialidad.valid) {
      if (this.isEdit) {
        this.especialidaService
          .putUpdateEspecialidad(this.item.idEsp, this.formEspecialidad.value)
          .subscribe({
            next: (val: any) => {
              if (val.affected === 1) {
                this.alert.sweet(
                  'Edición de Especialidad',
                  'Se realizó correctamente',
                  'success'
                );
                this.limpiar();
                this.especialidaService.refresh = true;
                this.isClicked = false;
                $('#modal-add-edit').modal('hide');
              } else {
                this.alert.sweet(
                  'Edición de Especialidad',
                  'No se pudo realizar la edición.',
                  'error'
                );
              }
            },
          });
      } else {
        this.especialidaService
          .postAddEspecialidad(this.formEspecialidad.value)
          .subscribe({
            next: (val: any) => {
              if (val) {
                this.alert.sweet(
                  'Registro de Especialidad',
                  'Se realizó correctamente',
                  'success'
                );
                this.limpiar();
                this.especialidaService.refresh = true;
              } else {
                this.alert.sweet(
                  'Registro de Especialidad',
                  'No se pudo realizar el registro.',
                  'error'
                );
                this.limpiar();
                this.especialidaService.refresh = true;
              }
            },
          });
        this.isClicked = false;
        $('#modal-add-edit').modal('hide');
      }
    }
  }

  recoveryEspecialidad() {
    if (this.item) {
      this.especialidaService.getByIDEspecialidad(this.item.idEsp).subscribe({
        next: (val: any) => {
          this.formEspecialidad.patchValue(val);
        },
      });
    }
  }

  limpiar() {
    this.formEspecialidad.reset();
    this.item = '';
    this.isClicked = false;
    this.clearItem.emit(true);
  }
}
