import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages.component';
import { NotFoundComponent } from '../shared/components/pagina-error/not-found/not-found.component';
import { HomeComponent } from '../shared/components/home/home.component';
import { controlGuard } from '../guards/control.guard';

const routes: Routes = [
  {
    path: '',
    component: PagesComponent,
    children: [
      { path: '', component: HomeComponent, canActivate: [controlGuard] },
      {
        path: 'tipos-atencion',
        loadChildren: () =>
          import('./tipos-atencion/tipos-atencion.module').then(
            (m) => m.TiposAtencionModule
          ),
        canActivate: [controlGuard],
      },
      {
        path: 'especialidades',
        loadChildren: () =>
          import('./especialidades/especialidades.module').then(
            (m) => m.EspecialidadesModule
          ),
        canActivate: [controlGuard],
      },
      {
        path: 'unidades',
        loadChildren: () =>
          import('./unidades/unidades.module').then((m) => m.UnidadesModule),
        canActivate: [controlGuard],
      },
      {
        path: 'carpetas',
        loadChildren: () =>
          import('./carpetas/carpetas.module').then((m) => m.CarpetasModule),
        canActivate: [controlGuard],
      },
      {
        path: 'subir-archivos',
        loadChildren: () =>
          import('./subir-archivos/subir-archivos.module').then(
            (m) => m.SubirArchivosModule
          ),
        canActivate: [controlGuard],
      },
      {
        path: 'buscar-archivos',
        loadChildren: () =>
          import('./buscar-archivos/buscar-archivos.module').then(
            (m) => m.BuscarArchivosModule
          ),
        canActivate: [controlGuard],
      },
      {
        path: 'prestamos',
        loadChildren: () =>
          import('./prestamos/prestamos.module').then((m) => m.PrestamosModule),
        canActivate: [controlGuard],
      },
      {
        path: 'reportes',
        loadChildren: () =>
          import('./reportes/reportes.module').then((m) => m.ReportesModule),
        canActivate: [controlGuard],
      },
      { path: '**', component: NotFoundComponent, canActivate: [controlGuard] },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {}
