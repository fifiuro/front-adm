import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AlertsService } from 'src/app/helpers/alerts/alerts.service';
import { TipoAtencionService } from 'src/app/services';
import { CaratulasService } from 'src/app/services/caratulas/caratulas.service';
import { EspecialidadService } from 'src/app/services/especialidades/especialidad.service';
import { PermisosService } from 'src/app/services/permisos/permisos.service';
import { PersonaUnidadService } from 'src/app/services/personaUnidad/persona-unidad.service';
import { FtpService } from 'src/app/services/subir-archivos/ftp/ftp.service';

@Component({
  selector: 'app-buscar-archivos',
  templateUrl: './buscar-archivos.component.html',
  styleUrls: ['./buscar-archivos.component.css'],
})
export class BuscarArchivosComponent {
  idPu!: number;
  idPer!: number;
  header: any;
  show: boolean = false;
  selectedTa: any;
  listTa: any;
  selectedEsp: any;
  listEsp: any;
  isClicked: boolean = false;
  cargando: boolean = false;
  archivos: any = [];
  pagination: number = 1;
  cantReg: number = 10;
  allArchivos: number = 0;
  public hasError: boolean = false;
  ruta!: string;

  constructor(
    private route: ActivatedRoute,
    private readonly personaUnidadService: PersonaUnidadService,
    private readonly tipoAtencionService: TipoAtencionService,
    private readonly especialidadService: EspecialidadService,
    private readonly caratulaService: CaratulasService,
    private readonly ftpService: FtpService,
    private readonly alert: AlertsService,
    private readonly permisosService: PermisosService
  ) {}

  ngOnInit() {
    this.idPu = parseInt(this.route.snapshot.paramMap.get('id')!);
    this.loadDataPersona(this.idPu);
    this.loadDataTIpoAtencion();
    this.loadDataEspecialidad();
  }

  verificar(compara: string) {
    return this.permisosService.verificarPermisos(compara);
  }

  get tipoAtencion() {
    return this.formBuscar.get('tipoAtencion') as FormControl;
  }

  get especialidad() {
    return this.formBuscar.get('especialidad') as FormControl;
  }

  get fIni() {
    return this.formBuscar.get('fIni') as FormControl;
  }

  get fFin() {
    return this.formBuscar.get('fFin') as FormControl;
  }

  formBuscar = new FormGroup({
    tipoAtencion: new FormControl('', Validators.required),
    especialidad: new FormControl('', Validators.required),
    fIni: new FormControl('', Validators.required),
    fFin: new FormControl('', Validators.required),
  });

  loadDataPersona(id: number) {
    this.personaUnidadService.getByIdPersonaUnidad(id).subscribe({
      next: (val: any) => {
        if (val) {
          this.header = val;
          this.idPer = val.persona.idPer;
          this.show = true;
        }
      },
    });
  }

  loadDataTIpoAtencion() {
    this.tipoAtencionService.getComboTipoAtencion().subscribe({
      next: (val: any) => {
        if (val) {
          this.listTa = val;
        } else {
        }
      },
    });
  }

  loadDataEspecialidad() {
    this.especialidadService.getComboEsepecialidad().subscribe({
      next: (val: any) => {
        if (val) {
          this.listEsp = val;
        } else {
        }
      },
    });
  }
  findFile() {
    let ta: any = this.formBuscar.value.tipoAtencion
      ? this.formBuscar.value.tipoAtencion
      : 0;
    let es: any = this.formBuscar.value.especialidad
      ? this.formBuscar.value.especialidad
      : 0;
    let fi: any = this.formBuscar.value.fIni ? this.formBuscar.value.fIni : 0;
    let ff: any = this.formBuscar.value.fFin ? this.formBuscar.value.fFin : 0;
    // if (this.formBuscar.valid) {
    this.caratulaService.getBuscar(this.idPer, ta, es, fi, ff).subscribe({
      next: (val: any) => {
        if (val) {
          this.archivos = val;
          this.allArchivos = 10;
          this.cargando = true;
          this.isClicked = false;
        } else {
          this.alert.sweet(
            'Busqueda de Archivos',
            'No se encontraron Resultados.',
            'error'
          );
        }
      },
    });
    // }
  }

  nomArchivo(ruta: string) {
    var r = ruta.split('*');
    return r[r.length - 1];
  }

  descargarBtn(item: string) {
    this.ruta = item;
    let nombre = this.nomArchivo(this.ruta);
    this.ftpService.downloadFile(nombre).subscribe({
      next: (archivo) => {
        const blob = new Blob([archivo], { type: 'application/pdf' });
        const url = window.URL.createObjectURL(blob);
        const enlace = document.createElement('a');
        enlace.href = url;
        enlace.target = '_blank';
        // enlace.download = nombre;
        enlace.click();
        window.URL.revokeObjectURL(url);
      },
    });
  }

  receivePath(event: any) {
    this.ruta = event;
    this.ftpService.downloadFileFTP(this.ruta).subscribe({
      next: (val: any) => {
        if (val.code == 226) {
          this.alert.sweet(
            'Descarga de Archivo',
            'Listo el archivo para la descarga.',
            'success'
          );
          this.descargarBtn(this.ruta);
        } else {
          this.alert.sweet(
            'Descarga de Archivo',
            'La descarga no se realzo.',
            'danger'
          );
        }
      },
    });
  }

  numRegistro() {
    this.cargando = false;
    this.pagination = 1;
    this.findFile();
  }

  renderPage(event: number) {
    this.cargando = false;
    this.pagination = event;
    this.findFile();
  }
}
