import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelAfiliadoComponent } from './sel-afiliado.component';

describe('SelAfiliadoComponent', () => {
  let component: SelAfiliadoComponent;
  let fixture: ComponentFixture<SelAfiliadoComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SelAfiliadoComponent]
    });
    fixture = TestBed.createComponent(SelAfiliadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
