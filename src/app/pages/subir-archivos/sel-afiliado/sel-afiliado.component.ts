import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AlertsService } from 'src/app/helpers/alerts/alerts.service';
import { SubirArchivosService } from 'src/app/services/subir-archivos/subir-archivos.service';
declare var $: any;

@Component({
  selector: 'app-sel-afiliado',
  templateUrl: './sel-afiliado.component.html',
  styleUrls: ['./sel-afiliado.component.css'],
})
export class SelAfiliadoComponent {
  @Output() afiliado = new EventEmitter();
  cargando: boolean = false;
  isClicked: boolean = false;
  afiliados: any = [];
  pagination: number = 1;
  cantReg: number = 10;
  allAfiliados: number = 0;
  public hasError: boolean = false;

  constructor(
    public subirArchivos: SubirArchivosService,
    private alert: AlertsService
  ) {}

  get ci() {
    return this.formAfiliado.get('ci') as FormControl;
  }

  get matricula() {
    return this.formAfiliado.get('matricula') as FormControl;
  }

  formAfiliado = new FormGroup({
    ci: new FormControl('', Validators.required),
    matricula: new FormControl('', Validators.required),
  });

  findAfiliado() {
    this.afiliados.splice(0, this.afiliados.length);
    this.isClicked = true;
    this.cargando = false;
    if (this.formAfiliado.valid) {
      this.subirArchivos
        .getFindAfiliado(
          String(this.formAfiliado.value.ci),
          String(this.formAfiliado.value.matricula)
        )
        .subscribe({
          next: (val: any) => {
            if (val.ok) {
              this.afiliados.push(val.datosAsegurado);
              this.allAfiliados = 10;
              this.cargando = true;
              this.isClicked = false;
            } else {
              this.alert.sweet('Busqueda de Afialidos', val.msg, 'error');
              this.isClicked = false;
            }
          },
        });
    }
  }

  selectAfialiado(item: any) {
    this.afiliado.emit(item);
    this.cargando = false;
    this.formAfiliado.reset();
    $('#modal-sel-afiliados').modal('hide');
    $('#modal-sel-unidad').modal('show');
  }

  numRegistro() {
    this.cargando = false;
    this.pagination = 1;
    this.findAfiliado();
  }

  renderPage(event: number) {
    this.cargando = false;
    this.pagination = event;
    this.findAfiliado;
  }

  cleaning() {
    this.cargando = false;
    this.isClicked = false;
    this.formAfiliado.reset();
  }
}
