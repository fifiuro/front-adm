import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SubirArchivosComponent } from './subir-archivos.component';
import { SubirArchivosListComponent } from './subir-archivos-list/subir-archivos-list.component';
import { ExploradorArchivosComponent } from './explorador-archivos/explorador-archivos.component';
import { BuscarArchivosComponent } from './buscar-archivos/buscar-archivos.component';

const routes: Routes = [
  {
    path: '',
    component: SubirArchivosComponent,
    children: [
      { path: '', redirectTo: 'subir-archivos-list', pathMatch: 'full' },
      { path: 'subir-archivos-list', component: SubirArchivosListComponent },
      {
        path: 'subir-archivos-form/:id',
        component: ExploradorArchivosComponent,
      },
      { path: 'buscar-archivos/:id', component: BuscarArchivosComponent },
      //   { path: 'tipo-atencion-detail/:userId', component: UsersDetailComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SubirArchivosRoutingModule {}
