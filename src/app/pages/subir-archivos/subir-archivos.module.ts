import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubirArchivosComponent } from './subir-archivos.component';
import { SubirArchivosListComponent } from './subir-archivos-list/subir-archivos-list.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { SubirArchivosRoutingModule } from './subir-archivos-routing.module';
import { SelAfiliadoComponent } from './sel-afiliado/sel-afiliado.component';
import { ExploradorArchivosComponent } from './explorador-archivos/explorador-archivos.component';

import { NgSelectModule } from '@ng-select/ng-select';
import { SelUnidadComponent } from './sel-unidad/sel-unidad.component';
import { UpLoadComponent } from './up-load/up-load.component';
import { BuscarArchivosComponent } from './buscar-archivos/buscar-archivos.component';
import { PathDownloadComponent } from './path-download/path-download.component';

@NgModule({
  declarations: [
    ExploradorArchivosComponent,
    SubirArchivosComponent,
    SelAfiliadoComponent,
    SubirArchivosListComponent,
    SelUnidadComponent,
    UpLoadComponent,
    BuscarArchivosComponent,
    PathDownloadComponent,
  ],
  imports: [
    SharedModule,
    CommonModule,
    SubirArchivosRoutingModule,
    NgxPaginationModule,
    NgSelectModule,
  ],
})
export class SubirArchivosModule {}
