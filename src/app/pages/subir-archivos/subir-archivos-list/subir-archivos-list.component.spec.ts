import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubirArchivosListComponent } from './subir-archivos-list.component';

describe('SubirArchivosListComponent', () => {
  let component: SubirArchivosListComponent;
  let fixture: ComponentFixture<SubirArchivosListComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SubirArchivosListComponent]
    });
    fixture = TestBed.createComponent(SubirArchivosListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
