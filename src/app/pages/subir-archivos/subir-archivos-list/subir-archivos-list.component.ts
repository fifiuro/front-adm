import { Component } from '@angular/core';
import { AlertsService } from 'src/app/helpers/alerts/alerts.service';
import { CarpetasService } from 'src/app/services/carpetas/carpetas.service';
import { PersonaUnidadService } from 'src/app/services/personaUnidad/persona-unidad.service';
import { PersonasService } from 'src/app/services/personas/personas.service';
import { FtpService } from 'src/app/services/subir-archivos/ftp/ftp.service';
import { TomoService } from 'src/app/services/subir-archivos/tomo/tomo.service';
import { ValidacionService } from 'src/app/helpers/validacion/validacion.service';
import { PermisosService } from 'src/app/services/permisos/permisos.service';
declare var $: any;

@Component({
  selector: 'app-subir-archivos-list',
  templateUrl: './subir-archivos-list.component.html',
  styleUrls: ['./subir-archivos-list.component.css'],
})
export class SubirArchivosListComponent {
  buscar = { matricula: '', ci: '' };
  estado = { activo: true };
  cargando = false;
  perUni: any = [];
  data: any = {
    nombres: '',
    apPaterno: '',
    apMaterno: '',
    nombreCompleto: '',
    matriculaTitular: '',
    matriculaBeneficiario: '',
    ciTitular: '',
    ciBeneficiario: '',
    complemento: '',
    expedido: '',
    fechaNac: '',
    edad: '',
    sexo: '',
    codPatronal: '',
    empresa: '',
    estadoAfiliacion: '',
    tipoAfiliacion: '',
    codigoDescripcion: '',
  };
  pagination: number = 1;
  cantReg: number = 10;
  allPerUni: number = 0;
  public hasError: boolean = false;

  // Creacion de las carpetas en el servidor de Archivos
  afiliado: any;
  carpeta: any;
  // FIN

  constructor(
    private personaService: PersonasService,
    private personaUnidadService: PersonaUnidadService,
    private carpetaServices: CarpetasService,
    private ftpService: FtpService,
    private tomoService: TomoService,
    private alert: AlertsService,
    private readonly validacionService: ValidacionService,
    private readonly permisosService: PermisosService,
  ) {}

  ngOnInit() {
    this.listPersonaUnidad();
  }

  verificar(compara: string) {
    return this.permisosService.verificarPermisos(compara);
  }

  receiveAfiliado($event: any) {
    this.afiliado = $event;
  }

  receiveCarpeta($event: any) {
    this.carpeta = $event;
    if (this.afiliado && this.carpeta) {
      this.matchData();
      this.personaService.postAddPersona(this.data).subscribe({
        next: (val: any) => {
          if (val) {
            this.personasUnidad(val.idPer);
          } else {
            this.alert.sweet(
              'Registro de Persona',
              'No se pudo registrar a la Persona.',
              'error'
            );
          }
        },
      });
    } else {
      this.alert.sweet(
        'Seleccionar Afiliado',
        'No se selecciono ningun Afiliado o Unidad.',
        'error'
      );
    }
  }

  matchData() {
    this.data.nombres = this.afiliado.ASE_NOM;
    this.data.apPaterno = this.afiliado.ASE_APAT;
    this.data.apMaterno = this.afiliado.ASE_AMAT;
    this.data.nombreCompleto =
      this.afiliado.ASE_NOM +
      ' ' +
      this.afiliado.ASE_APAT +
      ' ' +
      this.afiliado.ASE_AMAT;
    this.data.matriculaTitular = this.afiliado.ASE_MAT_TIT;
    this.data.matriculaBeneficiario = this.afiliado.ASE_MAT;
    this.data.ciTitular = this.afiliado.ASE_CI_TIT;
    this.data.ciBeneficiario = String(this.afiliado.ASE_CI);
    this.data.complemento = this.afiliado.ASE_CI_COM;
    this.data.expedido = String(this.afiliado.ASE_CIEXT);
    let f = new Date(this.afiliado.ASE_FEC_NAC);
    this.data.fechaNac = f;
    this.data.edad = this.afiliado.ASE_EDAD;
    if (this.afiliado.ASE_SEXO == 'F') {
      this.data.sexo = 'FEMENINO';
    } else if (this.afiliado.ASE_SEXO == 'M') {
      this.data.sexo = 'MASCULINO';
    }
    this.data.codPatronal = this.afiliado.EMP_NPATRONAL;
    this.data.empresa = this.afiliado.EMP_NOM;
    this.data.estadoAfiliacion = this.afiliado.ASE_ESTADO;
    this.data.tipoAfiliacion = this.afiliado.ASE_TIPO;
    if (this.afiliado.ASE_COND_EST == 'T') {
      this.data.codigoDescripcion = 'TITULAR';
    } else {
      this.data.codigoDescripcion = this.afiliado.ASE_COND_EST;
    }
    this.data.activo = true;
  }

  personasUnidad(idPer: number) {
    let data = {
      persona: idPer,
      unidad: this.carpeta.unidad.idUni,
    };
    this.personaUnidadService.postAddPersonaUnidad(data).subscribe({
      next: (val: any) => {
        if (val) {
          this.carpetasOtro(idPer, this.carpeta.idCar);
        }
      },
    });
  }

  carpetasOtro(idPer: number, idCarp: number) {
    let matricula: string;
    let ruta: string;
    let estructura: any;
    let tomo: any;
    let carpetas: string = '';
    this.personaService.getByIdPersona(idPer).subscribe({
      next: (val: any) => {
        matricula = val.matriculaTitular;
        this.carpetaServices.getByIDCarpeta(idCarp).subscribe({
          next: (val: any) => {
            ruta = val.ruta;
            estructura = JSON.parse(JSON.parse(val.estructura));
            // Desde aqui para llamar a la funcion de crear Carpetas
            estructura.forEach((element: any) => {
              if (element.children.length > 0) {
                element.children.forEach((c: any) => {
                  tomo = c.text;
                  if (c.children.length > 0) {
                    c.children.forEach((ch: any) => {
                      carpetas = carpetas + ch.text + ',';
                    });
                  }
                });
              }
            });
            carpetas = carpetas.substring(0, carpetas.length - 1);
            this.ftpService
              .createFolders(ruta, matricula, tomo, carpetas)
              .subscribe({
                next: (val: any) => {
                  if (val) {
                    this.createTomo(tomo, idPer);
                  }
                },
              });
          },
        });
      },
    });
  }

  createTomo(tomo: string, idPer: number) {
    let data = {
      tomo: tomo,
      fojas: 0,
      cd: 0,
      rx: 0,
      tomografia: 0,
      estado: 'ABIERTO',
      numero: 1,
      persona: idPer,
    };
    this.tomoService.postAddTomo(data).subscribe({
      next: (val: any) => {
        if (val) {
          this.alert.sweet(
            'Registro Afialiado',
            'Se creo el Registro y Estructura de carpetas correcatmente.',
            'success'
          );
          this.listPersonaUnidad();
        }
      },
    });
    return;
  }

  listPersonaUnidad() {
    this.personaUnidadService
      .getPersonalUnidad(this.pagination, this.cantReg)
      .subscribe({
        next: (res: any) => {
          this.perUni = res.data;
          this.allPerUni = res.count as number;
          this.cargando = true;
        },
      });
  }

  activeSubirArchivos(id: any, activo: boolean) {
    this.estado.activo = !activo;
    this.personaUnidadService
      .putUpdatePersonaUnidad(id, this.estado)
      .subscribe({
        next: (val: any) => {
          if (val.affected === 1) {
            this.alert.sweet(
              'Edición de Persona Unidad',
              'Se cambio de estado correctamente.',
              'success'
            );
            this.listPersonaUnidad();
          } else {
            this.alert.sweet(
              'Edición de Persona Unidad',
              'No se pudo realizar el cambio de estado.',
              'error'
            );
          }
        },
      });
  }

  findPersonaUnidad() {
    this.cargando = false;
    if (this.buscar.matricula || this.buscar.ci) {
      this.personaUnidadService
        .getLikePersonaUnidad(this.pagination, this.cantReg, this.buscar)
        .subscribe({
          next: (res: any) => {
            this.perUni = res.data;
            this.allPerUni = res.count;
            this.pagination = 1;
            this.cargando = true;
          },
        });
    } else {
      this.listPersonaUnidad();
    }
  }

  findRegionalByID(id: number, item: any) {}

  activeRegional(id: number, item: any) {}

  numRegistro() {
    this.cargando = false;
    this.pagination = 1;
    this.listPersonaUnidad();
  }

  upFilesArchivosClinicos(id: number, item: any) {}

  renderPage(event: any) {
    this.cargando = false;
    this.pagination = event;
    this.listPersonaUnidad();
  }

  validacionMatricula() {
    this.buscar.matricula = this.validacionService.validacionMatricula(
      this.buscar.matricula
    );
  }
}
