import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PathDownloadComponent } from './path-download.component';

describe('PathDownloadComponent', () => {
  let component: PathDownloadComponent;
  let fixture: ComponentFixture<PathDownloadComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PathDownloadComponent]
    });
    fixture = TestBed.createComponent(PathDownloadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
