import { Component, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
declare var $: any;

@Component({
  selector: 'app-path-download',
  templateUrl: './path-download.component.html',
  styleUrls: ['./path-download.component.css'],
})
export class PathDownloadComponent {
  @Output() path = new EventEmitter();
  isClicked: boolean = false;

  get ruta() {
    return this.formPath.get('ruta') as FormControl;
  }

  formPath = new FormGroup({
    ruta: new FormControl('', Validators.required),
  });

  sendRuta() {
    if (this.formPath.valid) {
      this.path.emit(this.formPath.value.ruta);
      this.cleaning();
      $('#modal-path-download').modal('hide');
    }
  }

  cleaning() {
    this.isClicked = false;
    this.formPath.reset();
  }
}
