import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AlertsService } from 'src/app/helpers/alerts/alerts.service';
import { TipoAtencionService } from 'src/app/services';
import { CaratulasService } from 'src/app/services/caratulas/caratulas.service';
import { EspecialidadService } from 'src/app/services/especialidades/especialidad.service';
import { FtpService } from 'src/app/services/subir-archivos/ftp/ftp.service';
import { TomoService } from 'src/app/services/subir-archivos/tomo/tomo.service';
declare var $: any;

@Component({
  selector: 'app-up-load',
  templateUrl: './up-load.component.html',
  styleUrls: ['./up-load.component.css'],
})
export class UpLoadComponent {
  @Input() idPer!: number;
  @Input() path!: string;
  @Input() currentFolder!: string;
  @Output() listaFolder = new EventEmitter<boolean>();
  currentTomo: string = '';
  tomos: any;
  especialidades: any;
  tiposAtencion: any;
  selectedTp: any;
  selectedEsp: any;
  selectedTom: any;
  isClicked: boolean = false;
  archivoCapturado: any;
  nombreArchivo!: string;
  buscarCie!: string;
  cies: any = [];

  constructor(
    private readonly tomoService: TomoService,
    private readonly especialidadService: EspecialidadService,
    private readonly tipoAtencionService: TipoAtencionService,
    private readonly caratulaService: CaratulasService,
    private readonly ftpService: FtpService,
    private readonly alert: AlertsService
  ) {}

  get fechaAtencion() {
    return this.formUpLoad.get('fechaAtencion') as FormControl;
  }

  get ruta() {
    return this.formUpLoad.get('ruta') as FormControl;
  }

  get fojas() {
    return this.formUpLoad.get('fojas') as FormControl;
  }

  get cd() {
    return this.formUpLoad.get('cd') as FormControl;
  }

  get rx() {
    return this.formUpLoad.get('rx') as FormControl;
  }

  get tomografia() {
    return this.formUpLoad.get('tomografia') as FormControl;
  }

  get tipoAtencion() {
    return this.formUpLoad.get('tipoAtencion') as FormControl;
  }

  get especialidad() {
    return this.formUpLoad.get('especialidad') as FormControl;
  }

  get tomo() {
    return this.formUpLoad.get('tomo') as FormControl;
  }

  get cie() {
    return this.formUpLoad.get('cie') as FormControl;
  }

  get buscar() {
    return this.formUpLoad.get('buscar') as FormControl;
  }

  formUpLoad = new FormGroup({
    fechaAtencion: new FormControl('', Validators.required),
    ruta: new FormControl('ruta'),
    fojas: new FormControl('', Validators.required),
    cd: new FormControl('', Validators.required),
    rx: new FormControl('', Validators.required),
    tomografia: new FormControl('', Validators.required),
    tipoAtencion: new FormControl('', Validators.required),
    especialidad: new FormControl('', Validators.required),
    tomo: new FormControl('1'),
    cie: new FormControl(''),
    buscar: new FormControl(''),
  });

  ngOnChanges() {
    if (this.idPer) {
      this.tomoService.getComboTomo(this.idPer).subscribe({
        next: (val: any) => {
          this.tomos = val;
        },
      });
      this.especialidadService.getComboEsepecialidad().subscribe({
        next: (val: any) => {
          this.especialidades = val;
        },
      });
      this.tipoAtencionService.getComboTipoAtencion().subscribe({
        next: (val: any) => {
          this.tiposAtencion = val;
        },
      });

      this.pathCurrent();
    }
  }

  pathCurrent() {
    let p = this.path.split('*');
    for (let i = 0; i < p.length; i++) {
      let t = p[i].indexOf('Tomo');
      if (t >= 0) {
        this.currentTomo = p[i];
      }
    }
  }

  createUpLoad() {
    this.isClicked = true;
    this.ftpService.upFile(this.path, this.archivoCapturado).subscribe({
      next: (val: any) => {
        if (val.code == '226') {
          this.formUpLoad.value.ruta = this.path + '*' + this.nombreArchivo;
          this.tomos.forEach((t: any) => {
            if (t.tomo === this.currentTomo) {
              this.formUpLoad.value.tomo = t.idTom;
              if (this.formUpLoad.valid) {
                this.caratulaService
                  .postAddCaratula(this.formUpLoad.value)
                  .subscribe({
                    next: (val: any) => {
                      if (val) {
                        this.listaFolder.emit();
                        let body = { fojas: val.fojas };
                        this.tomoService
                          .putupdateFojas(parseInt(val.tomo), body)
                          .subscribe({
                            next: (val: any) => {
                              if (val.affected == 1) {
                                this.alert.sweet(
                                  'Registro de Caratula',
                                  'Se realizó correctamente.',
                                  'success'
                                );
                              } else {
                                this.alert.sweet(
                                  'Registro de Caratula',
                                  'No se pudo actualizar las fojas correspondientes al Tomo.',
                                  'error'
                                );
                              }
                            },
                          });
                      } else {
                        this.alert.sweet(
                          'Registro de Caratula',
                          'No se pudo realizar el Resgistro.',
                          'error'
                        );
                      }
                    },
                  });
                this.limpiar();
                $('#modal-up-load').modal('hide');
              }
            }
          });
        } else {
          this.alert.sweet(
            'Subida de Archivo',
            'No se pudo realizar la subida del archivo.',
            'error'
          );
        }
      },
    });
  }

  capturarFile(event: any) {
    this.archivoCapturado = event.target.files[0];
    this.nombreArchivo = event.target.files[0].name;
  }

  findCie10() {
    if (this.buscarCie) {
      this.caratulaService.getFindCie(this.buscarCie).subscribe({
        next: (val: any) => {
          if (val.ok) {
            this.cies = val.diagnosticoCie10;
            this.cies.forEach((el: any) => {
              el.completo = el.CIE10_4DIG_COD + ' - ' + el.CIE10_4DIG;
            });
          }
        },
      });
    }
  }

  limpiar() {
    this.formUpLoad.reset();
    this.isClicked = false;
  }
}
