import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpLoadComponent } from './up-load.component';

describe('UpLoadComponent', () => {
  let component: UpLoadComponent;
  let fixture: ComponentFixture<UpLoadComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [UpLoadComponent]
    });
    fixture = TestBed.createComponent(UpLoadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
