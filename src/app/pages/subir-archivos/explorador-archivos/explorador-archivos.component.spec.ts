import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExploradorArchivosComponent } from './explorador-archivos.component';

describe('ExploradorArchivosComponent', () => {
  let component: ExploradorArchivosComponent;
  let fixture: ComponentFixture<ExploradorArchivosComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ExploradorArchivosComponent],
    });
    fixture = TestBed.createComponent(ExploradorArchivosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
