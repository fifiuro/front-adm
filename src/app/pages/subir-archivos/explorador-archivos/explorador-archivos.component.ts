import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertsService } from 'src/app/helpers/alerts/alerts.service';
import { CaratulasService } from 'src/app/services/caratulas/caratulas.service';
import { CarpetasService } from 'src/app/services/carpetas/carpetas.service';
import { PersonaUnidadService } from 'src/app/services/personaUnidad/persona-unidad.service';
import { FtpService } from 'src/app/services/subir-archivos/ftp/ftp.service';
import { TomoService } from 'src/app/services/subir-archivos/tomo/tomo.service';
declare var $: any;

@Component({
  selector: 'app-explorador-archivos',
  templateUrl: './explorador-archivos.component.html',
  styleUrls: ['./explorador-archivos.component.css'],
})
export class ExploradorArchivosComponent {
  idPu!: number;
  idPer!: number;
  header: any;
  show: boolean = false;
  path: string = '';
  currentFolder: string = '';
  matricula: string = '';
  listFolders: any;
  showCrearTomo: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private readonly personaUnidadService: PersonaUnidadService,
    private readonly carpetaService: CarpetasService,
    private readonly ftpService: FtpService,
    private readonly tomoService: TomoService,
    private readonly carpetaServices: CarpetasService,
    private readonly caratulasService: CaratulasService,
    private readonly alert: AlertsService
  ) {}

  ngOnInit() {
    this.idPu = parseInt(this.route.snapshot.paramMap.get('id')!);
    this.loadDataPersonaUnidad();
  }

  /**
   * Cargar los datos Persona y Unidad
   */
  loadDataPersonaUnidad() {
    this.personaUnidadService.getByIdPersonaUnidad(this.idPu).subscribe({
      next: (val: any) => {
        this.header = val;
        this.show = true;
        this.loadDataCarpeta(
          this.header.unidad.idUni,
          this.header.persona.matriculaBeneficiario
        );
      },
    });
  }

  /**
   * Carga los datos de Carpeta
   */
  loadDataCarpeta(id: number, matricula: string) {
    // Carga los Datos de Carpeta
    this.carpetaService.getByIDCarpeta(id).subscribe({
      next: (val: any) => {
        this.path = val.ruta + '*' + matricula;
        this.matricula = matricula;
        this.currentFolder = matricula;
        this.checkBtnTomo(this.path, this.matricula);
        this.listFolder('');
      },
    });
  }

  /**
   * Se hace la peticion por el contenido de un folder
   * @param folder
   */
  listFolder(folder: string) {
    let num = 0;
    if (folder) {
      this.path = this.path + '*' + folder;
      this.currentFolder = folder;
    }
    this.ftpService.getListFolder(this.path).subscribe({
      next: async (val: any) => {
        for (let i = 0; i < val.length; i++) {
          let ruta = this.path + '*' + val[i].name + '*';
          this.caratulasService.getByRutaLike(ruta).subscribe({
            next: (res: number) => {
              val[i].numArchivos = res;
            },
          });
        }
        this.listFolders = val;
        this.checkBtnTomo(this.path, this.matricula);
      },
    });
  }

  /**
   * Se sube un nivel y luego la peticion por el contenido
   * de un folder
   * @param ruta
   */
  backFolder(ruta: string) {
    if (this.currentFolder != this.matricula) {
      this.ftpService.getBackFolder(ruta).subscribe({
        next: (val: any) => {
          for (let i = 0; i < val.lista.length; i++) {
            let ruta = this.path + '*';
            let pathArray = this.path.split('*');
            let pa = pathArray!.pop();
            let pathUnido = pathArray.join('*');
            ruta = pathUnido + '*' + val.lista[i].name + '*';
            this.caratulasService.getByRutaLike(ruta).subscribe({
              next: (res: number) => {
                val.lista[i].numArchivos = res;
              },
            });
          }
          this.listFolders = val.lista;
          this.path = val.ruta;
          let ac = this.path.split('*');
          this.currentFolder = ac[ac.length - 1];
          this.checkBtnTomo(this.path, this.matricula);
        },
      });
    }
  }

  /**
   * Cambiar el nombre del Carpeta o Archivo
   * @param item
   */
  async renameFolder(item: any) {
    let newName = await this.alert.sweetInput(
      'Ingrese el nombre del Archivo',
      'Nombre del Archivo',
      item.name
    );
    if (newName) {
      let nombreActual = this.path + '*' + item.name;
      let nuevoNombre = this.path + '*' + newName;
      this.ftpService.renemaFolder(nombreActual, nuevoNombre).subscribe({
        next: (val: any) => {
          if (val.code == 250) {
            this.caratulasService.getByRutaCaratula(nombreActual).subscribe({
              next: (val: any) => {
                let data = {
                  ruta: nuevoNombre,
                };
                this.caratulasService
                  .putUpdateCaratula(val.idCar, data)
                  .subscribe({
                    next: (val: any) => {
                      if (val) {
                        this.alert.sweet(
                          'Cambiar Nombre',
                          'Se cambio de nombre correctamente.',
                          'success'
                        );
                        this.listFolderOut();
                      }
                    },
                  });
              },
            });
          } else {
            this.alert.sweet(
              'Cambiar Nombre',
              'No se pudo cambio nombre.',
              'error'
            );
          }
        },
      });
    } else {
      this.alert.sweet(
        'Cambiar Nombre',
        'El nombre del Archivo no debe ser vacio.',
        'error'
      );
    }
  }

  /**
   * Crear un Folder
   */
  async newFolder() {
    let newName = await this.alert.sweetInput(
      'Ingrese el nombre de la Nueva Carpeta',
      'Nombre de la Carpeta',
      ''
    );
    if (newName) {
      this,
        this.ftpService.createStructure(this.path, newName).subscribe({
          next: (val: any) => {
            if (val) {
              this.alert.sweet(
                'Crear Carpeta',
                'La Carpeta se creo correctamente.',
                'success'
              );
              this.listFolderOut();
            } else {
              this.alert.sweet(
                'Crear Carpeta',
                'La Carpeta no se pudo crear.',
                'error'
              );
            }
          },
        });
    }
  }

  /**
   * Eliminar una Carpeta o Archivo
   * @param item
   */
  async deleteFolder(item: any) {
    let ruta = this.path + '*' + item.name;
    let h = await this.alert.sweetConfirmDelete(
      '¿Esta seguro?',
      'De eliminar la Carpeta o Archivo!',
      'Si, Eliminar'
    );
    if (h) {
      this.ftpService.deleteFolder(this.path, item.name, item.type).subscribe({
        next: (val: any) => {
          this.listFolderOut();
          this.caratulasService
            .putEliminarCaratula(ruta, { activo: false })
            .subscribe({
              next: (val: any) => {
                if (val) {
                  console.log('Eliminado');
                }
              },
            });
        },
      });
    }
  }

  upLoad(idPer: number) {
    this.idPer = idPer;
    $('#modal-up-load').modal('show');
  }

  /**
   * Crear un nuevo Tomo el cual incluye una estructura
   * que esta definida en base de datos
   * @param idPer
   * @param idUni
   */
  newTomo(idPer: number, idUni: any) {
    let matricula: string;
    let ruta: string;
    let estructura: any;
    let tomo: any;
    let carpetas: string = '';

    this.carpetaServices.getByIDCarpeta(idUni).subscribe({
      next: (val: any) => {
        ruta = val.ruta;
        estructura = JSON.parse(JSON.parse(val.estructura));
        // Desde aqui para llamar a la funcion de crear Carpetas
        estructura.forEach((element: any) => {
          if (element.children.length > 0) {
            element.children.forEach((c: any) => {
              if (c.children.length > 0) {
                c.children.forEach((ch: any) => {
                  carpetas += ch.text + ',';
                });
              }
            });
          }
        });
        carpetas = carpetas.substring(0, carpetas.length - 1);
        this.tomoService.getByIdPersona(idPer).subscribe({
          next: (val: any) => {
            let num = val.numero + 1;
            let nomTomo = 'Tomo' + num;
            tomo = nomTomo;
            this.ftpService
              .createTomo(ruta, this.matricula, tomo, carpetas)
              .subscribe({
                next: (val: any) => {
                  if (val) {
                    this.createTomo(tomo, num, idPer);
                  }
                },
              });
          },
        });
      },
    });
  }

  /**
   * Crear el registro en base de datos de la carpetas Tomo
   * en el servidor FTP
   * @param tomo
   * @param num
   * @param idPer
   * @returns
   */
  createTomo(tomo: string, num: number, idPer: number) {
    let data = {
      tomo: tomo,
      fojas: 0,
      cd: 0,
      rx: 0,
      tomografia: 0,
      estado: 'ABIERTO',
      numero: num,
      persona: idPer,
    };
    this.tomoService.postAddTomo(data).subscribe({
      next: (val: any) => {
        if (val) {
          this.alert.sweet(
            'Registro Afialiado',
            'Se creo el Registro y Estructura de carpetas correcatmente.',
            'success'
          );
          this.listFolderOut();
        }
      },
    });
    return;
  }

  /**
   * Verifica que estamos en la carpeta principal y que
   * el boton sea visible
   * @param path
   * @param matricula
   */
  checkBtnTomo(path: string, matricula: string) {
    let r = path.split('*');
    if (r[r.length - 1] === matricula) {
      this.showCrearTomo = false;
    } else {
      this.showCrearTomo = true;
    }
  }

  /**
   * Verifica que el nombre de la carpeta no sea Tomo
   */
  isProtect(name: string) {
    let posicion = name.indexOf('Tomo');
    if (posicion >= 0) {
      return false;
    } else {
      return true;
    }
  }

  listFolderOut() {
    let original = this.path.split('*');
    let ultimo = original.pop();
    let p = original.join('*');
    this.path = p;
    let a = this.listFolder(ultimo + '');
  }

  async numArchivos(item: any): Promise<any> {
    let ruta = this.path + '*' + item.name + '*';
    let numero = 0;
    await this.caratulasService.getByRutaLike(ruta).subscribe({
      next: (val: number) => {
        numero = val;
        return numero;
      },
    });
  }
}
