import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelUnidadComponent } from './sel-unidad.component';

describe('SelUnidadComponent', () => {
  let component: SelUnidadComponent;
  let fixture: ComponentFixture<SelUnidadComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SelUnidadComponent]
    });
    fixture = TestBed.createComponent(SelUnidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
