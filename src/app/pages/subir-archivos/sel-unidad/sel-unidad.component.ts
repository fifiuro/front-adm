import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CarpetasService } from 'src/app/services/carpetas/carpetas.service';
declare var $: any;

@Component({
  selector: 'app-sel-unidad',
  templateUrl: './sel-unidad.component.html',
  styleUrls: ['./sel-unidad.component.css'],
})
export class SelUnidadComponent {
  @Output() carpeta = new EventEmitter();
  selectedUni: any;
  unidades: any;

  constructor(private carpetaService: CarpetasService) {}

  ngOnInit() {
    this.carpetaService.getComboCarpeta().subscribe({
      next: (val: any) => {
        this.unidades = val;
      },
    });
  }

  selectUnidad() {
    this.carpetaService.getByIDCarpeta(this.selectedUni).subscribe({
      next: (val: any) => {
        this.carpeta.emit(val);
        $('#modal-sel-unidad').modal('hide');
      },
    });
  }
}
