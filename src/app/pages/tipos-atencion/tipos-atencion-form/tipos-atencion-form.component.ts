import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { TipoAtencionService } from 'src/app/services';
import { AlertsService } from 'src/app/helpers/alerts/alerts.service';
declare var $: any;

@Component({
  selector: 'app-tipos-atencion-form',
  templateUrl: './tipos-atencion-form.component.html',
  styleUrls: ['./tipos-atencion-form.component.css'],
})
export class TiposAtencionFormComponent {
  @Input() titulo!: string;
  @Input() isEdit!: boolean;
  @Input() item: any;
  @Output() clearItem = new EventEmitter<boolean>();
  isClicked: boolean = false;

  constructor(
    public tipoAtencionService: TipoAtencionService,
    private alert: AlertsService
  ) {}

  ngOnChanges() {
    this.recoveryTipoAtencion();
  }

  get tipo() {
    return this.formTipoAtencion.get('tipo') as FormControl;
  }

  formTipoAtencion = new FormGroup({
    tipo: new FormControl('', Validators.required),
  });

  createTipoAtencion() {
    this.isClicked = true;
    if (this.formTipoAtencion.valid) {
      if (this.isEdit) {
        this.tipoAtencionService
          .putUpdateTipoAtencion(this.item.idTip, this.formTipoAtencion.value)
          .subscribe({
            next: (val: any) => {
              if (val.affected === 1) {
                this.alert.sweet(
                  'Edición de Tipo de Atención',
                  'Se realizó correctamente.',
                  'success'
                );
                this.limpiar();
                this.tipoAtencionService.refresh = true;
                this.isClicked = false;
                $('#modal-add-edit').modal('hide');
              } else {
                this.alert.sweet(
                  'Edición de Tipo de Atención',
                  'No se pudo realizar la edición.',
                  'error'
                );
                this.limpiar();
                this.tipoAtencionService.refresh = true;
                this.isClicked = false;
                $('#modal-add-edit').modal('hide');
              }
            },
          });
      } else {
        this.tipoAtencionService
          .postAddTipoatencion(this.formTipoAtencion.value)
          .subscribe({
            next: (val: any) => {
              if (val) {
                this.alert.sweet(
                  'Registro de Tipo de Atención',
                  'Se realizó correctamente.',
                  'success'
                );
                this.limpiar();
                this.tipoAtencionService.refresh = true;
              } else {
                this.alert.sweet(
                  'Registro de Tipo de Atención',
                  'No se pudo realizar el registro.',
                  'error'
                );
                this.limpiar();
                this.tipoAtencionService.refresh = true;
              }
            },
          });
        this.isClicked = false;
        $('#modal-add-edit').modal('hide');
      }
    }
  }

  recoveryTipoAtencion() {
    if (this.item) {
      this.tipoAtencionService.getByIDTipoAtencion(this.item.idTip).subscribe({
        next: (val: any) => {
          this.formTipoAtencion.patchValue(val);
        },
      });
    }
  }

  limpiar() {
    this.formTipoAtencion.reset();
    this.item = '';
    this.isClicked = false;
    this.clearItem.emit(true);
  }
}
