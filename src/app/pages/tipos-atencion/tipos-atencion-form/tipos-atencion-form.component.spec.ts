import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TiposAtencionFormComponent } from './tipos-atencion-form.component';

describe('TiposAtencionFormComponent', () => {
  let component: TiposAtencionFormComponent;
  let fixture: ComponentFixture<TiposAtencionFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TiposAtencionFormComponent]
    });
    fixture = TestBed.createComponent(TiposAtencionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
