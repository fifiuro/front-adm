import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TiposAtencionComponent } from './tipos-atencion.component';
import { TiposAtencionListComponent } from './tipos-atencion-list/tipos-atencion-list.component';

const routes: Routes = [
  {
    path: '',
    component: TiposAtencionComponent,
    children: [
      { path: '', redirectTo: 'tipo-atencion-list', pathMatch: 'full' },
      { path: 'tipo-atencion-list', component: TiposAtencionListComponent },
      //   { path: 'tipo-atencion-detail/:userId', component: UsersDetailComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TiposAtencionRoutingModule {}
