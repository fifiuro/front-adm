import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TiposAtencionComponent } from './tipos-atencion.component';

describe('TiposAtencionComponent', () => {
  let component: TiposAtencionComponent;
  let fixture: ComponentFixture<TiposAtencionComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TiposAtencionComponent]
    });
    fixture = TestBed.createComponent(TiposAtencionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
