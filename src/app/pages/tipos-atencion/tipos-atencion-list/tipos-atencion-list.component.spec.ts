import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TiposAtencionListComponent } from './tipos-atencion-list.component';

describe('TiposAtencionListComponent', () => {
  let component: TiposAtencionListComponent;
  let fixture: ComponentFixture<TiposAtencionListComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TiposAtencionListComponent]
    });
    fixture = TestBed.createComponent(TiposAtencionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
