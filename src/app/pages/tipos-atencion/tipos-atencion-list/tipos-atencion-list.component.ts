import { Component } from '@angular/core';
import { AlertsService } from 'src/app/helpers/alerts/alerts.service';
import { TipoAtencionInterface } from 'src/app/interfaces';
import { TipoAtencionService } from 'src/app/services';
import { PermisosService } from 'src/app/services/permisos/permisos.service';
declare var $: any;
@Component({
  selector: 'app-tipos-atencion-list',
  templateUrl: './tipos-atencion-list.component.html',
  styleUrls: ['./tipos-atencion-list.component.css'],
})
export class TiposAtencionListComponent {
  buscar = { tipoAtencion: '' };
  estado = {
    activo: true,
  };
  cargando = false;
  tipoAtencion!: any[];
  pagination: number = 1;
  cantReg: number = 10;
  allTipoAtencion: number = 0;
  public hasError: boolean = false;

  titulo = '';
  isEdit: boolean = false;
  item: any;

  constructor(
    public tiposAtencionService: TipoAtencionService,
    private readonly permisosService: PermisosService,
    public alert: AlertsService
  ) {}

  ngOnInit() {
    this.listTipoAtencion();
  }

  verificar(compara: string) {
    return this.permisosService.verificarPermisos(compara);
  }

  ngDoCheck() {
    if (this.tiposAtencionService.refresh) {
      this.numRegistro();
      this.tiposAtencionService.refresh = false;
    }
  }

  listTipoAtencion() {
    this.tiposAtencionService
      .getTipoAtencion(this.pagination, this.cantReg)
      .subscribe({
        next: (res: any) => {
          this.tipoAtencion = res.data;
          this.allTipoAtencion = res.count as number;
          this.cargando = true;
        },
      });
  }

  findTipoAtencion() {
    this.cargando = false;
    if (this.buscar.tipoAtencion) {
      this.tiposAtencionService
        .getLikeTipoaAtencion(
          this.pagination,
          this.cantReg,
          this.buscar.tipoAtencion
        )
        .subscribe({
          next: (res: any) => {
            this.tipoAtencion = res.data;
            this.allTipoAtencion = res.count;
            this.pagination = 1;
            this.cargando = true;
          },
        });
    } else {
      this.listTipoAtencion();
    }
  }

  findTipoAtencionByID(item: any) {
    this.isEdit = true;
    this.item = item;
    $('#modal-add-edit').modal('show');
  }

  activeTipoAtencion(id: any, activo: any) {
    this.estado.activo = !activo;
    this.tiposAtencionService.putUpdateTipoAtencion(id, this.estado).subscribe({
      next: (val: any) => {
        if (val.affected === 1) {
          this.alert.sweet(
            'Edición de Tipo de Atención',
            'Se cambio de estado correctamente.',
            'success'
          );
          this.listTipoAtencion();
        } else {
          this.alert.sweet(
            'Edición de Tipo de Atención',
            'No se pudo realizar el cambio de estado.',
            'error'
          );
        }
      },
    });
  }

  numRegistro() {
    this.cargando = false;
    this.pagination = 1;
    this.listTipoAtencion();
  }

  renderPage(event: number) {
    this.cargando = false;
    this.pagination = event;
    this.listTipoAtencion();
  }

  formAddTipoAtencion(titulo: boolean) {
    this.isEdit = false;
    $('#modal-add-edit').modal('show');
  }

  limpiar(e: boolean) {
    this.item = '';
  }
}
