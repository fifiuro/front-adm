import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TiposAtencionComponent } from './tipos-atencion.component';
import { TiposAtencionListComponent } from './tipos-atencion-list/tipos-atencion-list.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { TiposAtencionRoutingModule } from './tipos-atencion-routing.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { TiposAtencionFormComponent } from './tipos-atencion-form/tipos-atencion-form.component';

@NgModule({
  declarations: [
    TiposAtencionComponent,
    TiposAtencionListComponent,
    TiposAtencionFormComponent,
  ],
  imports: [
    SharedModule,
    CommonModule,
    TiposAtencionRoutingModule,
    NgxPaginationModule,
  ],
})
export class TiposAtencionModule {}
