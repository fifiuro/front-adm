import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PublicComponent } from './public.component';
import { SharedModule } from '../shared/shared.module';
import { PublicRoutingModule } from './public-routing.module';

@NgModule({
  declarations: [PublicComponent],
  imports: [CommonModule, SharedModule, PublicRoutingModule],
})
export class PublicModule {}
