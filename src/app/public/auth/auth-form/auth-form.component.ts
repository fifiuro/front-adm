import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
declare var $: any;
@Component({
  selector: 'app-auth-form',
  templateUrl: './auth-form.component.html',
  styleUrls: ['./auth-form.component.css'],
})
export class AuthFormComponent {
  isCLicked: boolean = false;
  ngOnInit() {}

  constructor(private authService: AuthService, private router: Router) {}

  get user() {
    return this.formLogin.get('user') as FormControl;
  }

  get pass() {
    return this.formLogin.get('pass') as FormControl;
  }

  formLogin = new FormGroup({
    user: new FormControl('', Validators.required),
    pass: new FormControl('', Validators.required),
  });

  login() {
    this.isCLicked = true;
    if (this.formLogin.valid) {
      this.authService
        .login(this.formLogin.value.user!, this.formLogin.value.pass!)
        .subscribe({
          next: (val: any) => {
            if (val) {
              this.authService.saveData('jwt', val.jwt);
              this.authService.saveData('id_usuario', String(val.id_usuario));
              this.authService.saveData('nom_usuario', val.nom_usuario);
              this.authService.saveData('nom_completo', val.nom_completo);
              this.authService.saveData('rol', val.rol);
              this.authService.saveData('change', '2');
              // Logueo al sistema que me trae los datos
              this.authService
                .loginSistema(
                  this.formLogin.value.user!,
                  this.formLogin.value.pass!
                )
                .subscribe({
                  next: (res: any) => {
                    if (res) {
                      this.authService.saveData('jwtSis', res.token);
                      this.router.navigate(['/pages']);
                    }
                  },
                });
              // Fin
            }
          },
        });
      // Fin
    }
  }
}
