import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthFormComponent } from './auth-form/auth-form.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuthRoutingModule } from './auth-routing.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { AuthComponent } from './auth.component';

@NgModule({
  declarations: [AuthFormComponent, AuthComponent],
  imports: [SharedModule, CommonModule, AuthRoutingModule, NgxPaginationModule],
})
export class AuthModule {}
