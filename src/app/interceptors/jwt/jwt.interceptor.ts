import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/services/auth/auth.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(public authService: AuthService) {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    let token;
    let id = this.authService.getData('id_usuario');
    let body: any = request.body;
    if (request.method == 'POST' || request.method == 'PUT') {
      body.idUsuarioRegistro = id;
    }
    if (this.authService.getData('change') == '1') {
      if (this.authService.getData('jwt')) {
        token = this.authService.getData('jwt');
        request = request.clone({
          body: body,
          setHeaders: {
            jwt: token,
          },
        });
      }
    } else if (this.authService.getData('change') == '2') {
      if (this.authService.getData('jwtSis')) {
        token = this.authService.getData('jwtSis');
        request = request.clone({
          setHeaders: {
            Authorization: 'Bearer ' + token,
          },
        });
      }
    }
    return next.handle(request);
  }
}
