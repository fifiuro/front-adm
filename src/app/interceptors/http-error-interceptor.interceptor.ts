import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable, catchError, throwError } from 'rxjs';
import { AlertsService } from '../helpers/alerts/alerts.service';
import { AuthService } from '../services/auth/auth.service';
import { Router } from '@angular/router';

@Injectable()
export class HttpErrorInterceptorInterceptor implements HttpInterceptor {
  constructor(
    private readonly router: Router,
    private alert: AlertsService,
    private readonly authService: AuthService
  ) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError((httpErrorResponse: HttpErrorResponse) => {
        let errorMensaje = '';
        let errorType = '';
        let errorDescription = '';

        if (httpErrorResponse.error instanceof ErrorEvent) {
          errorMensaje = httpErrorResponse.error.error;
        } else {
          if (httpErrorResponse.status === 0) {
            errorType = 'Error del lado del Servidor';
            errorMensaje = 'No hay conexion con el servidor.';
          } else {
            errorType = 'Error';
            errorMensaje = `${httpErrorResponse.status}: ${httpErrorResponse.error.error}`;
            errorDescription = `${httpErrorResponse.error.message}`;
          }
          errorType = errorType + ' : ' + errorDescription;
          this.alert.toast(errorMensaje, errorType, 'error');
          if (errorDescription == 'El Token ha expirado') {
            this.authService.clearAllData();
            this.router.navigate(['public/']);
          }
        }
        return throwError(() => new Error(errorMensaje));
      })
    );
  }
}
