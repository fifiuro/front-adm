export interface RoleInterface {
  createdAt?: Date;
  updatedAt?: Date;
  activo?: boolean;
  idUsuarioModificacion?: number;
  idUsuarioRegistro?: number;
  idRol?: number;
  rol?: string;
  descripcion?: string;
  especial?: string;
}
