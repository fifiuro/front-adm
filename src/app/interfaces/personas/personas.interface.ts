export interface PersonasInterface {
  ASE_NOM?: string;
  ASE_APAT?: string;
  ASE_AMAT?: string;
  ASE_MAT_TIT?: string;
  ASE_MAT?: string;
  ASE_CI_TIT?: null;
  ASE_CI?: number;
  ASE_CI_COM?: null;
  ASE_CIEXT?: string;
  ASE_FEC_NAC?: Date;
  ASE_EDAD?: number;
  ASE_SEXO?: string;
  ASE_TIPO?: string;
  ASE_ESTADO?: string;
  ASE_COND_EST?: string;
  EMP_NPATRONAL?: string;
  EMP_NOM?: string;
}
