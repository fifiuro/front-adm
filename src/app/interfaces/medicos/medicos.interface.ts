export interface MedicosInterface {
  apMaterno?: string;
  apPaterno?: string;
  especialidad?: string;
  nombres?: string;
}
