export interface EspecialidadesInterface {
  createdAt?: Date;
  updatedAt?: Date;
  activo?: boolean;
  idUsuarioRegistro?: number;
  idEsp?: number;
  especialidad?: string;
}
