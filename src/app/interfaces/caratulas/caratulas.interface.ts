import { EspecialidadesInterface } from '../especialidades/especialidades.interface';
import { TipoAtencionInterface } from '../tipos-atencion/tipo-atencion.interface';
import { TomoInterface } from '../tomo/tomo.interface';

export interface CaratulasInterface {
  createdAt?: Date;
  updatedAt?: Date;
  activo?: boolean;
  idUsuarioRegistro?: number;
  idCar?: number;
  fechaAtencion?: Date;
  ruta: string;
  fojas: number;
  cd: number;
  rx: number;
  tomografia: number;
  tipoAtencion: TipoAtencionInterface;
  especialidad: EspecialidadesInterface;
  tomo: TomoInterface;
}
