export interface UnidadesInterface {
  createdAt?: Date;
  updatedAt?: Date;
  activo?: boolean;
  idUsuarioRegistro?: number;
  idUni?: number;
  unidad?: string;
  sigla?: string;
}
