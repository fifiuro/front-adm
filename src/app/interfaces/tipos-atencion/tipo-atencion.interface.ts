export interface TipoAtencionInterface {
  createdAt?: Date;
  updatedAt?: Date;
  activo?: boolean;
  idUsuarioRegistro?: number;
  idTip?: number;
  tipo?: string;
}
