import { MedicosInterface } from '../medicos/medicos.interface';
import { PersonasInterface } from '../personas/personas.interface';
import { TomoInterface } from '../tomo/tomo.interface';

export interface PrestamosInterface {
  createdAt?: Date;
  updatedAt?: Date;
  activo?: boolean;
  idUsuarioRegistro?: number;
  fechaPrestamo?: Date;
  horaPrestamo?: string;
  obsPrestamo?: string;
  fechaDevolucion?: Date;
  horaDevolucion?: string;
  obsDevolucion?: string;
  persona: PersonasInterface;
  tomo: TomoInterface;
  medico: MedicosInterface;
}
