export interface TomoInterface {
  createdAt?: Date;
  updatedAt?: Date;
  activo?: boolean;
  idUsuarioRegistro?: number;
  idTom?: number;
  tomo?: number;
  fojas?: number;
  cd?: number;
  rx?: number;
  tomografia?: number;
  estado?: number;
}
