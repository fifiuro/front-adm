import { UnidadesInterface } from "../unidades/especialidades.interface";

export interface CarpetasInterface {
  createdAt?: Date;
  updatedAt?: Date;
  activo?: boolean;
  idUsuarioRegistro?: number;
  idCar?: number;
  estructura?: Estructura;
  ruta?: string;
  unidad?: UnidadesInterface;
}

export interface Estructura {
  carpeta?: string;
  subcarpeta?: Estructura[];
}
