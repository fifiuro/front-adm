export interface PersonaUnidadInterface {
  createdAt?: Date;
  updatedAt?: Date;
  activo?: boolean;
  idUsuarioRegistro?: number;
  idPu?: number;
  personaIdPer?: number;
  unidadIdUni?: number;
}
