import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ValidacionService {
  constructor() {}

  validacionMatricula(buscar: string) {
    let m = buscar;
    let newMatricula = '';
    for (let i = 0; i < m.length; i++) {
      const element = m[i];
      if (i == 2) {
        if (m[i] == '-') {
          newMatricula += '-';
        } else {
          newMatricula += '-' + m[i];
        }
      } else if (i == 7) {
        if (m[i] == ' ') {
          newMatricula += ' ';
        } else {
          newMatricula += ' ' + m[i];
        }
      } else {
        newMatricula += m[i];
      }
    }
    return newMatricula;
  }
}
